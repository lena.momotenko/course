const defaults = {
  name : 'lena',
  text : 'text',
  avatarUrl : 'https://scontent-frt3-1.xx.fbcdn.net/v/t1.0-9/30698236_1050937658394952_2608633086789694996_n.jpg?_nc_cat=108&_nc_oc=AQkS6qy9vasBjm9Gb0E2ZAY3b5x4T_BUIIcp-oqkziGIujMisiZkwpiKT47PZ-WI5xc&_nc_ht=scontent-frt3-1.xx&oh=f9a05b3400d92cc8a3bb41b41d5cc290&oe=5DB05AB3',
}

class ConstComment  {
  constructor(options = defaults) {
    this.options = Object.assign({}, defaults, options);
    this.name = this.options.name;
    this.text = this.options.text;
    this.avatarUrl = this.options.namavatarUrl;
  }

  show() {
    console.log(this.name);
  }
}

const constComment = new ConstComment;
constComment.show();

document.addEventListener('DOMContentLoaded', function(){

  // let constComment1 = new ConstComment('Lena', 'test', 'test', 1);
  // let constComment2 = new ConstComment('Roma', 'test', 'test', 2);
  // let constComment3 = new ConstComment('Sasha', 'test', 'test', 3);
  // let constComment4 = new ConstComment('Tanya', 'test', 'test', 3);

  // let comments = [];
  // comments.push(constComment1, constComment2, constComment3, constComment4);

  // function Comment (comments) {
  //   for(key in comments) {
  //     console.log(comments[key]);
  //     console.log(comments[key].name)
  //   }
    
  // }

  // let comment = new Comment(comments);
});

/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/