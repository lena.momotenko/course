document.addEventListener('DOMContentLoaded', function(){
    const Train = {
       name: 'Hogwarts Express',
       speed: '112.2 km/h',
       passagers: '1000',
       move: function(){
         console.log( `Поезд ${this.name} везет ${this.passagers} пассажиров со скоростью ${this.speed}`);
       },
       stop: function(){
         this.speed = '0';
         console.log( `Поезд ${this.name} остановился. Скорость ${this.speed}`);
       },
       add: function(newPassagers = 0, leavePassagers = 0){
         this.passagers = +this.passagers + newPassagers - leavePassagers;
         console.log( `Поезд ${this.name} везет ${this.passagers}. Скорость ${this.speed}`);
       }
    };

    Train.move();
    Train.stop();
    Train.add(20, 40);

});
/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
