document.addEventListener('DOMContentLoaded', function(){
  function Dog(name, breed, status) {
    this.name = name;
    this.breed = breed;
    this.status = status;
    this.SayHello = function () {
      console.log(`Dog's name is ${this.name}`);
    }
  }

  let dog = new Dog('K9', 'Mops', 'Wild');
  dog.SayHello();

  dog.showProp = function () {
    for (prop in dog) {
      console.log(dog[prop] );
    }
  }

  let showProp = dog.showProp.bind(dog);

  showProp();
});


/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства




    Dog {
      name: '',
      breed: '',
      status: 'idle',

      changeStatus: function(){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/
