document.addEventListener('DOMContentLoaded', function(){
// import createElement from '../../../helper/createElement';
function createElement (element = 'div', className = '', text = '', parent = document.body) {
  let newElement = document.createElement(element);
  newElement.classList.add(className);
  newElement.innerText = text;
  parent.appendChild(newElement);
}

/*
    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();
*/

  let colors = {
    background: 'purple',
    color: 'white',
    myCall: function (color, text){
      console.log(this);
      console.log(colors);
      document.body.style.background = this.background;
      document.body.style.color = color;
      createElement('h1', 'title', text);
    },
    myBind: function (){
      console.log(this);
      console.log(colors);
      document.body.style.background = this.background;
      document.body.style.color = this.color;
      createElement('h1', 'title', 'I know how binding works in JS');
    },
    myApply: function (text){
      console.log(this);
      console.log(colors);
      document.body.style.background = this.background;
      document.body.style.color = this.color;
      console.log(text);
      createElement('h1', 'title', text);
    },
  }

  let arrIknow = ['I', 'know', 'how', 'applying', 'works', 'in', 'JS'];
  arrIknow.push('or not?')
  var arr = [];
  arr.push(1);
  arr.push(5);
  arr.push(2);
  // colors.myCall.call( colors, 'red', arrIknow.join(' ') );
  colors.myCall.call( colors, Math.max(1, 5, 2) );
  let myBind = colors.myBind.bind(colors);
  // myBind();
  colors.myApply.apply( colors, arr );


  // Example 
  var user = {
    firstName: "Вася",
    sayHi: function(who) {
      console.log( this.firstName, who);
    }
  };
  let app = ['привет', 'xb'];
  setTimeout(user.sayHi.call(user, app), 1000); // undefined (не Вася!)
});
