/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/

document.addEventListener('DOMContentLoaded', () => {
  class Posts {
    constructor (options) {
			this.isActive = options.isActive ? options.isActive : false;
			this._id = options._id;
			this.title = options.title;
			this.about = options.about;
			this.created_at = options.created_at;
			this.like = options.like ? options.like : 0;
		}

		addLike (target) {
			target.addEventListener('click', () => {
				this.like += 1;
				target.value = this.like;
				let currentObj = JSON.parse(localStorage.getItem(this._id));
				currentObj.like = this.like;
				localStorage.setItem(currentObj._id, JSON.stringify(currentObj));
			});
		}

		publishPost (target) {
			target.addEventListener('click', () => {
				let currentObj = JSON.parse(localStorage.getItem(this._id));
				currentObj.isActive = true;
				localStorage.setItem(currentObj._id, JSON.stringify(currentObj));
				target.remove();
			});
		}

		deletePost (target, parent) {
			target.addEventListener('click', () => {
				parent.remove();
				localStorage.removeItem(this._id);
			});
		}

		createBlock (html, className, parentClass, isDelete = true, isPublish = false, isLike = false) {
			const parent = document.getElementById(parentClass);
			const newPost = document.createElement('div');
			newPost.classList.add(className);
			newPost.setAttribute('id', this._id)
			newPost.innerHTML = html;
			parent.insertBefore(newPost, parent.childNodes[2]);
			if (isDelete) {
				const deleteBtn = newPost.querySelector('.delete');
				this.deletePost(deleteBtn, newPost);
			}
			if (isPublish) {
				const publishBtn = newPost.querySelector('.publish');
				this.publishPost(publishBtn, newPost);
			}
			if (isLike) {
				const likeBtn = newPost.querySelector('.like');
				this.addLike(likeBtn);
			}
		}

		render () {
			if (this.isActive) {
				const newPostHTML = `
					<h2>${this.title}</h2>
					<p>${this.about}</p>
					<p class='date'>Posted ${this.created_at} | ID: #${this._id}</p>
					<input class='like' type='button' value='${this.like}'>
					<input class='delete' type='button' value='Delete'>
				`;
				this.createBlock(newPostHTML, 'post', 'root', true, false, true);
			} else {
				const newDraftHTML = `
					<h2>${this.title}</h2>
					<p>${this.about}</p>
					<p class='date'>Posted ${this.created_at} | ID: #${this._id}</p>
					<input class='delete' type='button' value='Delete'>
					<input class='publish' type='button' value='Publish after reload'>
				`;
				this.createBlock(newDraftHTML, 'draft', 'drafts', true, true, false);
			}
		}
		
		init() {
			this.render();
		}
	}

	/* start: render by onload */
		for (var key in localStorage) {
		let publishedPostObject;
		if(localStorage.hasOwnProperty(key)) {
			if (localStorage[key][0] === '{') { /* Я устала, а иначе если в локалсторадже была запись с предыдущего занятия, ничего не работало */
				publishedPostObject = JSON.parse(localStorage[key]);
				let savedPosts = new Posts(publishedPostObject);
				savedPosts.init();
			}
			
		}
	}
 	/* end: render by onload */

	const formCreateNewPost = document.forms.createNewPost;  
	const title = formCreateNewPost.elements.title;
	const about = formCreateNewPost.elements.about;
	const _id = formCreateNewPost.elements._id;
	const btns =  formCreateNewPost.querySelectorAll('button');

	function getData(date) {
		date = date ? date : new Date();
		const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}, ${date.getHours()}:${date.getMinutes()}`
	}

	let postObject = {};
	let isValid = false;
	let length = 0;

	function createPost(obj) {
		_id.value = obj._id;
		localStorage.setItem(obj._id, JSON.stringify(obj));
		const posts = new Posts(obj);
		posts.init();
	}
		
	function savePost(published) {
		postObject = {
			_id: _id.value ? _id.value : Date.now(),
			title: title.value,
			about: about.value,
			created_at: getData(),
			isActive: published ? true : false,
		};
		createPost(postObject);
		if (published) {
			postObject = {};
		}
	}

	function clearPost() {
		_id.value = '';
		title.value = '';
		about.value = '';
		postObject = {};
	}

	function generatePost() {
		function status(response) {
			if (response.status >= 200 && response.status < 300) {
				return Promise.resolve(response)
			} else {
				return Promise.reject(new Error(response.statusText))
			}
		}
		function json(response) {
			return response.json()
		}
		fetch('http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2')
		.then(status)
		.then(json)
		.then(function(data) {
			if (length < data.length) {
				let parseDate = getData(new Date(Date.parse(data[length].created_at.replace(/\s+/g, ''))));
				data[length].created_at = parseDate;
				createPost(data[length]);
				length += 1;
			}
		}).catch(function(error) {
			console.warn('Request failed', error);
		});
	}

	btns.forEach(function(btn) {
		btn.addEventListener('click', () => {
			title.value && about.value ? isValid = true : isValid = false;
			if (btn.id === 'publish' && isValid) {
				savePost(true);
				clearPost();
			} else if (btn.id === 'draft' && isValid) {
				savePost();
			} else if (btn.id === 'clear') {
				clearPost();
			} else if (btn.id === 'generate') {
				generatePost();
			}
		});
	});
});