document.addEventListener('DOMContentLoaded', () => {
  function getColor () {
    function getRandomPart(min = 0, max = 256) {
      let number = Math.floor(Math.random() * (max - min)) + min;  
      return number
    }
    return `rgb(${getRandomPart()}, ${getRandomPart()}, ${getRandomPart()})`
	}
	
	let savedColor = localStorage.getItem('bg');
	if (savedColor ) {
		document.body.style.backgroundColor = savedColor;	
	}
	
	const newColorBtn = document.getElementById('newColor');
	newColorBtn.addEventListener('click', () => {
		let color = getColor();
		document.body.style.backgroundColor = color;
		localStorage.setItem('bg', color);
	});
});

/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/