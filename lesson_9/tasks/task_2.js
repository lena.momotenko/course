/*
    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/

document.addEventListener('DOMContentLoaded', () => {
	let userDataFromBackend = 'eyJsb2dpbiI6ImFkbWluQGV4YW1wbGUuY29tIiwicGFzc3dvcmQiOiIxMjM0NTY3OCJ9';

	class Authentication {
		constructor () {
			this.isAuth = localStorage.getItem('auth') ? true : false;
			this.root = document.getElementById('root');
		}

		signIn () {
			let renderForm = `
			<form action="" name="loginForm">
				<label for="login"></label>
				<input type="text" name="login" id="login" placeholder="Login">
				<label for="password"></label>
				<input type="password" name="password" id="password" placeholder="password">
				<button id="signIn" type="submit">Sign In</button>
				<p id="message"></p>
			</form>
			`;

			this.root.innerHTML = renderForm;

			const form = document.forms.loginForm;  
			const loginField = form.elements.login;
			const passwordField = form.elements.password;
			const message = document.getElementById('message');

			form.addEventListener('submit', e => {
				e.preventDefault();
				let userData = {
					login: loginField.value,
					password: passwordField.value,
				}
				userData = btoa(JSON.stringify(userData));
				if (userDataFromBackend === userData) {
					localStorage.setItem('auth', userData);
					this.welcome();
				} else {
					message.innerHTML = 'No match';
				}
			});
		}

		exit (target) {
			target.addEventListener('click', () => {
				localStorage.removeItem('auth');
				this.signIn();
			});
		}

		welcome () {
			let authData = JSON.parse(atob(localStorage.getItem('auth')));
			let renderWelcome = `
				<h2>Hello, ${authData.login}!</h2>
				<button id="exit">Exit</button>
			`;
			this.root.innerHTML = renderWelcome;
			const exitBtn = document.getElementById('exit');
			this.exit(exitBtn);
		}

		init () {
			this.isAuth ? this.welcome() : this.signIn();
		}
	}

	const authentication = new Authentication();
	authentication.init();
});