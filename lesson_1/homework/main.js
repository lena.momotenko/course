document.addEventListener('DOMContentLoaded', function() {

  function getRandomColor () {
    var wrap = document.querySelector('.wrap');
    var checkbox = document.querySelector('.js-checkbox > input');
    var format = 'rgb';

    function getRandomPart(min = 0, max = 256) {
      var number = Math.floor(Math.random() * (max - min)) + min;  

      if (format === 'hex') {
        number = number.toString(16);
        number = ('0' + number).slice(-2);
      }
      
      return number
    }

    function getColorRgb (r,g,b) {
      return 'rgb(' + r + ',' + g + ',' + b + ')'
    }

    function getColorHex (r,g,b) {
      return '#' + r + g + b;
    }
    
    function getColor (format = rgb) {
      var option = {
        hex: getColorHex,
        rgb: getColorRgb
      }
      
      return option[format](getRandomPart(), getRandomPart(), getRandomPart());
    }

    function createNewElement(typeParam, wrapParam, classNameParam, textParam) {
      wrapParam ? wrapParam = wrapParam : wrapParam = wrap;

      var element = document.createElement(typeParam);
      element.className = classNameParam;
      element.innerText = textParam;
      wrapParam.insertBefore(element, wrapParam.firstChild);
    }

    createNewElement('p', false, 'js-color-text color', 'You can change color');
    createNewElement('button', false, 'js-button button', 'Click Me');
    
    var btn = document.querySelector('.js-button');
    var colorText = document.querySelector('.js-color-text');

    btn.addEventListener('click', function() {
      var bgColor = getColor(format);
      wrap.style.background = bgColor;
      colorText.innerHTML = 'Your color is: ' + bgColor;
    });

    checkbox.addEventListener('change', function() {
      this.checked ? format = 'hex' : format = 'rgb';
    });
  }

  getRandomColor();
});

/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
