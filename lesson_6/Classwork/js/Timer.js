/*
  Создайте счетчик секунд, который начинает отчет с 1 и заканчивается на 30,
  также добавьте кнопки который останавливают отчет, и запускают его заново и добавьте кнопку обнуляющую отчет.
  Также попробуйте изменить код так чтобы отчет начиналься с 30 и заканчивалься на 1.

  + бонус: Сделать так что бы на каждый тик (1с/2c) менялся цвет фона. Можно на основе генератора случайного цвета с предудущих уроков.
  + бонус: Сделать визуализацию стрелки которая идет по кругу
           document.getElementById("myDIV").style.transform = "rotate(7deg)";
  + бонус: Сделать кнопки которые выбирают режим в котором идет отсчет - обычный от 0 до 30 или реверсивный от 30 до 0
 */


document.addEventListener('DOMContentLoaded', () => {
  const defaults = {
    count: 0,
    intervalTime: 500,
    step: 1
  }
  const startBtn = document.getElementById('start');
  const stopBtn = document.getElementById('stop');
  const resetBtn = document.getElementById('reset');
  const reverseBtn = document.getElementById('reverse');

  class Timer {
    constructor(optionsDefaults = defaults) {
      const options = Object.assign({}, defaults, optionsDefaults);
      this.intervalTime = options.intervalTime;
      this.step = options.step;
      this.count = options.count;
      this.timer = null;
      this.isReverse = false;
      this.result = document.getElementById('js-result');
      this.endPoint = 10;
      this.button = undefined;
    }

    tiktak(count) {
      this.result.innerHTML = count;
    }

    btnDisabled() {
      startBtn.disabled = true;
      reverseBtn.disabled = true;
    }

    btnShow() {
      startBtn.disabled = false;
      reverseBtn.disabled = false;
    }

    finishState () {
      clearInterval(this.timer);
      this.isReverse = false;
      this.btnShow();
    }

    start() {
      this.btnDisabled();
      this.timer = setInterval(() => {
        this.tiktak(this.count);
        console.log(this.count);
        if(this.isReverse && this.count >= 0) {
          if (this.count == 0) {
            this.finishState();
          }
          this.count -= this.step;
        } else {
          if (this.count == this.endPoint) {
            this.finishState();
          }
          this.count += this.step;
        }
      }, this.intervalTime);
    }

    stop() {
      clearInterval(this.timer);
      this.btnShow();
    }

    reset() {
      clearInterval(this.timer);
      this.count = 0;
      this.tiktak(this.count);
      this.btnShow();
    }

    reverse() {
      this.isReverse = true;
      this.count = this.endPoint;
      this.start();
    }
  }
  
  const timer = new Timer({
    count: 1,
  });

  startBtn.addEventListener('click', event => { timer.start() });
  stopBtn.addEventListener('click', event => { timer.stop() });
  resetBtn.addEventListener('click', event => { timer.reset() });
  reverseBtn.addEventListener('click', event => { timer.reverse() });
});
