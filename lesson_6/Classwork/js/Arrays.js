document.addEventListener('DOMContentLoaded', () => {
  /*
  Задание:
  1. При помощи методов изложеных ниже, переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
    + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
    + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.
  */

  const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];
  const ITEA_COURSES_LENGTH = ITEA_COURSES.map(function(elem) {
    return elem.length;
  });
  let sourceArray = document.getElementById('sourceArray');
  sourceArray.innerHTML = ITEA_COURSES;
  let length = document.getElementById('length');
  length.innerHTML = ITEA_COURSES_LENGTH;

  /* ======== */

  function sortedTask () {
    let ul = document.getElementById('_sort');
    ITEA_COURSES.forEach(elem => {
      let item = document.createElement('li');
      item.innerHTML = elem;
      ul.appendChild(item);
    });
    function sortedArray () {
      ITEA_COURSES.sort();
      while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
      }
      ITEA_COURSES.forEach(elem => {
        let item = document.createElement('li');
        item.innerHTML = elem;
        ul.appendChild(item);
      });
    }

    const sortedBtn = document.getElementById('sortedBtn');
    sortedBtn.addEventListener('click', sortedArray);
  }
  sortedTask();

  /* ======== */

  const findInput = document.getElementById('findInput');
  const ul = document.getElementById('_find');
  const findBtn = document.getElementById('findBtn');

  function findInArray () {
    while (ul.hasChildNodes()) {
      ul.removeChild(ul.firstChild);
    }
    let value = findInput.value;
    if (value) {
      ITEA_COURSES.forEach(elem => {
        if (elem.toLowerCase().includes(value.toLowerCase())) {
          let item = document.createElement('li');
          item.innerHTML = elem;
          ul.appendChild(item);
        }
      });
    }
  }
  findBtn.addEventListener('click', findInArray);

  /*
  Задание 2:
  Написать фильтр массива по:
  Актеру, продолжительности
  Бонус:
    Сделать графическую оболочку для программы:
    - Селект со списком актеров
    - Селект или инпут с продолжительностью
    - Результат в виде списка фильмов
  */

  let stars = ['Elijah Wood','Ian McKellen','Orlando Bloom','Ewan McGregor','Liam Neeson','Natalie Portman','Ewan McGregor','Billy Bob Thornton','Martin Freeman'];
  let movies = [
      {
        name: 'Lord of the Rigs',
        duration: 240,
        starring: ['Elijah Wood', 'Ian McKellen', 'Orlando Bloom']
      },
      {
        name: 'Star Wars: Episode I - The Phantom Menace',
        duration: 136,
        starring: ['Ewan McGregor', 'Liam Neeson', 'Natalie Portman']
      },
      {
        name: 'Fargo',
        duration: 100,
        starring: ['Ewan McGregor', 'Billy Bob Thornton', 'Martin Freeman']
      },
      {
      name: 'V for Vendetta',
      duration: 150,
      starring: ['Hugo Weaving', 'Natalie Portman', 'Rupert Graves']
    }
  ];

  function findMovie() {
    function createElem (htmlElem, className, parent, text) {
      let elem = document.createElement(htmlElem);
      elem.classList.add(className);
      text ? elem.innerHTML = text : elem.innerHTML = '';
      parent.appendChild(elem);

      return elem
    }

    let movieBlock = document.getElementById('_movieBlock');
    let select = document.getElementById('_select');
    let input = document.getElementById('_duration');
    let btnFind = createElem('button', '_button', movieBlock, 'Find');
    let movieBlockResult = document.getElementById('_movieBlockResult');

    stars.forEach(function(item){
      createElem('option', '_option', select, item);
    });

    btnFind.addEventListener('click', () => {
      let actor = select.value;
      let duration = input.value;
      let moviesShortedThen = [];
      while (movieBlockResult.hasChildNodes()) {
        movieBlockResult.removeChild(movieBlockResult.firstChild);
      }

      if (duration) {
        movies.forEach(function(elem){
          if(elem.duration <= duration) {
            moviesShortedThen.push(elem);
          } 
        });
      } 

      if (actor) {
        let array;
        function isValue(element) {
          if (element.includes(actor)) {
            return element
          }
        }
        duration ? array = moviesShortedThen : array = movies;
        array.forEach(function(elem){
          if (elem.starring.some(isValue)) {
            let resultHTML = `
              Name: <b>${elem.name}</b><br>
              Duration: ${elem.duration} minutes<br>
              Starring: ${elem.starring}
            `;
            createElem('p', '_p', movieBlockResult, resultHTML);
          } 
        });
      } 
    });
  }
  findMovie();
});