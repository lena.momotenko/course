/*
    Создайте функцию, которая по клику на "показать день недели" возвращает день недели для выбранной даты с инпута.
    День нужно возвратить в div#result в текстовом формате т.е понедельник, вторник, суббота и т.д

    Это я хотела разобраться разобраться с методами и дестрактингом, дестрактинг классная штука 
*/
document.addEventListener('DOMContentLoaded', () => {
  
  function Weekday () {

    let selector = {
      myDateSelector: document.getElementById('MyDateSelector'),
      result: document.getElementById('result'),
    }

    let static = {
      days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Friday', 'Saturday']
    }
    
    this.getValue = function() {
      let { myDateSelector, result } = selector;
      let { days } = static;
      let dateValue = myDateSelector.value;
      if (dateValue) {
        let dayIndex = new Date(dateValue).getDay();
        result.innerHTML = `Your day is ${days[dayIndex]}`;
      } else {
        result.innerHTML = 'Choose some date';
      }
    }
  }

  const weekday = new Weekday();
  const btn = document.getElementById('ShowDayButton');
  btn.addEventListener('click', weekday.getValue);
  
});