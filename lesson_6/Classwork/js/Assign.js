/*
Задание:

  1. Используя интерфейс который есть на страничке, написать визуальный
  конструктор обектов используя значение key-value, которые вводятся в
  соответствующие поля в форме.

  2. Каждый созданый обьект выводить в тег code после добавления каждого
  нового поля. Т.е мы должны визуально видить в каком состоянии наши обьекты
  сейчас

  3. По нажатию кнопки Combine Objects! два обьекта должны обьедениться
  используя метод Object.assing или spread operator и вывестись на стринчку.

  * Изменять HTML можно.
*/
document.addEventListener('DOMContentLoaded', () => {
  const ObjForm1 = document.forms.ObjForm1;
  const ObjForm2 = document.forms.ObjForm2;

  function AddNewParam (form) {
    this.one = form.elements.key.value;
    this.two = form.elements.value.value;

    this.dataObject = {};
    console.log(this.one);
    this.dataObject.one = this.two;

    // return dataObject

    
  }

  ObjForm1.addEventListener('submit', () => {
    let dataObject = new AddNewParam(ObjForm1);
    console.log(dataObject.dataObject);
  });

  ObjForm2.addEventListener('submit', () => {
    let dataObject = new AddNewParam(ObjForm2);
    console.log(dataObject.dataObject);
  });
});  