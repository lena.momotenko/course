const Composition = () => {

  const makeBackEndMagic = (state) => ({
    makeBackEndMagic: ( arg ) => console.log(`${state.name} Makes Back End Magic ${arg ?  'and ' + arg : ''}`)
  });

  const makeFrontEndMagic = (state) => ({
    makeFrontEndMagic: () => console.log(`${state.name} Makes Front End Magic`)
  });

  const MakeItLooksBeautiful = (state) => ({
    MakeItLooksBeautiful : () => console.log(`${state.name} Makes It Looks Beautiful`)
  });

  const DistributeTasks = (state) => ({
    DistributeTasks: () => console.log(`${state.name} Distributes Tasks`)
  });

  const DrinkSomeTea = (state) => ({
    DrinkSomeTea: () => console.log(`${state.name} Drinks Some Tea`)
  });

  const WatchYoutube = (state) => ({
    WatchYoutube: () => console.log(`${state.name} Watches Youtube`)
  });

  const Procrastinate = (state) => ({
    Procrastinate: () => console.log(`${state.name} Procrastinates`)
  });

  class Employee {
    constructor (name, gender, age) {
      this.name = name;
      this.gender = gender;
      this.age = age;
    }
  }

  class BackEndDeveleper extends Employee {
    constructor(name, gender, age) {
      super(name, gender, age);
      this.type = 'backend';
      
      return Object.assign(this, makeBackEndMagic(this), DrinkSomeTea(this), Procrastinate(this));
    }

  }

  class FrontEndDeveleper extends Employee {
    constructor(name, gender, age) {
      super(name, gender, age);
      this.type = 'frontend';

      return Object.assign(this, makeFrontEndMagic(this), DrinkSomeTea(this), WatchYoutube(this));
    }
  }

  class Designer extends Employee {
    constructor(name, gender, age) {
      super(name, gender, age);
      this.type = 'design';

      return Object.assign(this, MakeItLooksBeautiful(this), WatchYoutube(this), Procrastinate(this));
    }
  }

  class ProjectManager extends Employee {
    constructor(name, gender, age) {
      super(name, gender, age);
      this.type = 'project';

      return Object.assign(this, DistributeTasks(this), Procrastinate(this),  DrinkSomeTea(this));
    }
  }
}

export default Composition;

export class HeadHunt {
    createTable () {
      let table = document.createElement('table');
      table.classList.add('js-head-hunt-table')
      document.body.appendChild(table);
    }
    hire (element) {
      const newEmployee = {
        backend : new BackEndDeveleper(element.name, element.gender, element.age),
        frontend: new FrontEndDeveleper(element.name, element.gender, element.age),
        design: new Designer(element.name, element.gender, element.age),
        project: new ProjectManager(element.name, element.gender, element.age),
      }

      if (element.type) {
        return newEmployee[element.type];
      } else {
        return
      }
    }
    render (element) {
      let node = document.createElement('tr');
      let table = document.querySelector('.js-head-hunt-table');
      node.innerHTML = `<td><b>Name:</b> ${element.name}</td><td><b>Age:</b> ${element.age}</td><td><b>Gender:</b> ${element.gender}</td><td><b>Type:</b> ${element.type}</td>`;
      table.appendChild(node);
    }
  }



