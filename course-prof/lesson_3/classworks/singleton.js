/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5

*/

const _data = { 
  laws: [{
    id: 0,
    text: 'abcdef'
  }],
  budget: 1000000,
  citizensSatisfactions: 0,
};

const government = {
  add: item => {
    _data.laws.push(item);
    _data.citizensSatisfactions += -10;
    console.log('New Law:', item);
  },
  reedAll: () => {
    _data.laws.forEach((law) => {
      console.log('Law:', law);
    });
  },
  reed: (id) => {
    console.log(`Law ID #${id}:`, _data.laws[id].text);
  },
  showLevel: () => {
    console.log('Satisfaction:', _data.citizensSatisfactions);
  },
  celebrate: () => {
    _data.citizensSatisfactions += 5;
    _data.budget += -5000;
    console.log('Satisfaction:', _data.citizensSatisfaction, '| Budget:', _data.budget);
  }
};

Object.freeze(government);

const singleton = () => {
  government.add({id: 1, text: 'qwerty'});
  government.add({id: 2, text: 'zxcvbn'});
  government.add({id: 3, text: 'poiuyt'});
  government.reedAll();
  government.reed(3);
  government.showLevel();
  government.celebrate();
  government.celebrate();
  government.celebrate();
  // government.meeting = 'Do Nothing'
  console.log(government);
}

export default singleton;

