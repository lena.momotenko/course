/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/hof.js":
/*!****************************!*\
  !*** ./application/hof.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n\n  Higher Order Functions\n  function is a values\n\n*/\nlet hofDemo = () => {\n  // let multiply = x => x * x;\n  // let nine = multiply(3);\n  // console.log( 'multiply:', nine );\n\n  /*\n    Array.filter (так же как map, forEach, etc...) пример использования HOF в нативном js\n    Паттерн позворялет использовать композцицию что бы собрать из маленьких функций одну большую\n  */\n  // let zoo = [\n  //   {id:0, name:\"WoofMaker\", species: 'dog'},\n  //   {id:1, name:\"WhiteFurr\", species: 'rabbit'},\n  //   {id:2, name:\"MeowMaker\", species: 'cat'},\n  //   {id:3, name:\"PoopMaker\", species: 'dog'},\n  //   {id:4, name:\"ScratchMaker\", species: 'cat'},\n  // ];\n  // let isDog = animal => animal.species === 'dog';\n  // let isCat = animal => animal.species === 'cat';\n  // let dogs = zoo.filter( isDog );\n  // let cats = zoo.filter( isCat );\n  // console.log('Here dogs:', dogs);\n  // console.log('Here cats:', cats);\n  // - - - - - - - - - - - - - - - - - -\n  // function compose(func_a, func_b){\n  //   return function(c){\n  //     return func_a( func_b(c) );\n  //   }\n  // }\n  const compose = (func_a, func_b) => c => func_a(func_b(c));\n\n  const addTwo = value => {\n    console.log('Add', value);\n    return value + 2;\n  };\n\n  const multiplyTwo = value => {\n    console.log('Mulitple', value);\n    return value * 2;\n  };\n\n  const addTwoAndMultiplayTwo = compose(multiplyTwo, addTwo);\n  console.log(addTwoAndMultiplayTwo);\n  console.log(addTwoAndMultiplayTwo(10));\n  /*\n    В данном случае происходит следующее:\n    - Вызывается ф-я compose которая принимает ф-и addTwo, multiplyTwo как аргументы\n    - Вызывается функция которая передана как аргумент func_b\n    - Результат её выполнения передается в функция func_a\n    - Общий результат возвращается в ф-ю которая нам возвращается в переменную\n  */\n  // console.log(\n  //   addTwoAndMultiplayTwo(2),\n  //   addTwoAndMultiplayTwo(6),\n  //   addTwoAndMultiplayTwo(40)\n  // );\n  // ---\n  // const doSmsng = (event) => {\n  //   console.log('do smsng', event);\n  //   return () => console.log('another func');\n  // }\n  // function doSmsng( status ){\n  //   if( status ){\n  //     return function(e){\n  //       console.log('status is true', status, 'e', e);\n  //     }\n  //   } else {\n  //     return function(e){\n  //       console.log('status is false', status, 'e', e);\n  //     }\n  //   }\n  // } \n  // const doSmsng = ( status ) => (e) => {\n  //   console.log('status', status, 'e', e);\n  // }\n  // const btn = document.getElementById('btn');\n  // const btn1 = document.getElementById('btn1');\n  // btn.addEventListener('click', doSmsng(false) );\n  // btn1.addEventListener('click', doSmsng(true) );\n  // ---\n  // class Item {\n  //   constructor(id){\n  //     this.id = id;\n  //     this.clickHandler = this.clickHandler.bind(this);\n  //   }\n  //   clickHandler(id) {\n  //     return (e) => {\n  //       console.log( 'this', this, 'id', id, 'event', e);\n  //     }\n  //   }\n  // clickHandler = id => e => {\n  //   console.log( 'this', this, 'id', id, 'event', e);\n  // }\n  //   render(){\n  //     const item = document.getElementById('class-item');\n  //     let node = document.createElement('div');\n  //         node.innerHTML = `\n  //           <div class=\"item\" data-id=\"${this.id}\">\n  //               <h2>${this.id}</h2>\n  //               <button class=\"_btn\"> Like </button>\n  //           </div>\n  //         `;  \n  //         node.querySelector('._btn').addEventListener('click', this.clickHandler( 10 ) );\n  //         item.appendChild(node);  \n  //   }\n  // }\n  // let firstItem = new Item(1);\n  // firstItem.render();\n  // let secondItem = new Item(2);\n  // secondItem.render();\n  // let thirdItem = new Item(3);\n  // thirdItem.render();\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (hofDemo);\n\n//# sourceURL=webpack:///./application/hof.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _singleton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleton */ \"./application/singleton/index.js\");\n/* harmony import */ var _hof__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hof */ \"./application/hof.js\");\n/* harmony import */ var _classworks_objectfreeze__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../classworks/objectfreeze */ \"./classworks/objectfreeze.js\");\n/* harmony import */ var _classworks_singleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../classworks/singleton */ \"./classworks/singleton.js\");\n/* harmony import */ var _classworks_composition__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../classworks/composition */ \"./classworks/composition.js\");\n/*\n  npm run cli -> Для запуска сборки\n*/\n\n\n\n\n // singtone();\n// hof();\n// objectfreeze();\n\nconsole.log(_classworks_composition__WEBPACK_IMPORTED_MODULE_4__[\"default\"]);\n_classworks_composition__WEBPACK_IMPORTED_MODULE_4__[\"default\"]; // let headHunt = new HeadHunt();\n// headHunt.createTable();\n\nlet Vadim = new BackEndDeveleper('Vadim', 'male', 30);\nlet Dima = new FrontEndDeveleper('Dima', 'male', 26);\nlet Diana = new Designer('Diana', 'female', 26);\nlet Lera = new ProjectManager('Lera', 'female', 28); // headHunt.render(Vadim);\n// headHunt.render(Dima);\n// headHunt.render(Diana);\n// headHunt.render(Lera);\n// Vadim.makeBackEndMagic('frontEnd');\n// Lera.DistributeTasks();\n\nconst getData = () => {\n  fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2').then(function (response) {\n    return response.json();\n  }).then(function (employeesJson) {\n    employeesJson.forEach(element => {\n      headHunt.hire(element);\n      headHunt.render(element);\n    });\n  });\n};\n\ndocument.querySelector('.js-new-employees').addEventListener('click', getData);\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/singleton/index.js":
/*!****************************************!*\
  !*** ./application/singleton/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _object_freeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./object_freeze */ \"./application/singleton/object_freeze.js\");\n/* harmony import */ var _old_singleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./old-singleton */ \"./application/singleton/old-singleton.js\");\n/* harmony import */ var _new_singleton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-singleton */ \"./application/singleton/new-singleton.js\");\n/* harmony import */ var _oop_singleton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./oop-singleton */ \"./application/singleton/oop-singleton.js\");\n\n\n\n // FreezeDemo();\n\nconst SingletonDemo = () => {\n  /*\n      Singleton (он же одиночка)— это паттерн проектирования,\n      который гарантирует, что у класса есть только один экземпляр,\n      и предоставляет к нему глобальную точку доступа.\n  \n      Про паттерн: https://refactoring.guru/ru/design-patterns/singleton\n  \n      Рассмотрим три реализации синглтона в JS:\n  */\n\n  /*\n  \tПервая -- классический ООП паттерн. Который актуален для большинства объектно ориентированых языков.\n  */\n  // \tconst store1 = new Single('test');\n  // \tstore1.addToData();\n  // \tconsole.log('first instance:', store1 );\n  // \tconst store2 = new Single('test2');\n  // \tstore2.addToData();\n  // \tconsole.log('second instanse:', store2 );\n  // \tconsole.log('is same Object?', store1 === store2 );\n\n  /*\n  \tВторая, старая реализация объектного синглтона через замыкания.\n  */\n  // oldSingletonDemo();\n\n  /*\n  \tТретья, новая реализация объектного синглтона через модули и метод Object.freeze();\n  */\n  // newSingetonDemo();\n};\n\nconst oldSingletonDemo = () => {\n  // Смотрим реализацию в файле old-singleton.js  \n  console.log(_old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n  _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({\n    id: 0,\n    language: 'js'\n  });\n  _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({\n    id: 1,\n    language: 'phyton'\n  });\n  _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({\n    id: 2,\n    language: 'php'\n  });\n  _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({\n    id: 3,\n    language: 'ruby'\n  });\n  console.log('OldSingleton', _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].showData());\n  let myLang = _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(0);\n  let myLang2 = _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(3);\n  console.log('OldSingleton -> myLang', myLang);\n  console.log('OldSingleton -> myLang', myLang2);\n}; //\n\n\nconst newSingetonDemo = () => {\n  // Как и все в js в 2019 меньше, быстрее, чище!\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({\n    id: 0,\n    language: 'js'\n  });\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({\n    id: 1,\n    language: 'phyton'\n  });\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({\n    id: 2,\n    language: 'php'\n  });\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({\n    id: 3,\n    language: 'ruby'\n  }); //\n\n  console.log('NewSingleton', _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"]);\n  let myLang = _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].get(2);\n  console.log('NewSingleton -> myLang', myLang);\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  console.log(_new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getCounter());\n  /*\n  \tДемо усложнить синглтон\n  */\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (SingletonDemo);\n\n//# sourceURL=webpack:///./application/singleton/index.js?");

/***/ }),

/***/ "./application/singleton/new-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/new-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// Так как является константой, не может быть измененно\nconst _data = {\n  store: [],\n  counter: 0\n}; // Создаем обьект и методы\n\nconst Store = {\n  add: item => _data.store.push(item),\n  get: id => _data.store.find(d => d.id === id),\n  showAllLang: () => [..._data.store],\n  getCounter: () => _data.counter,\n  addCounter: num => _data.counter += num\n}; // Замораживаем\n\nObject.freeze(Store);\n/* harmony default export */ __webpack_exports__[\"default\"] = (Store);\n\n//# sourceURL=webpack:///./application/singleton/new-singleton.js?");

/***/ }),

/***/ "./application/singleton/object_freeze.js":
/*!************************************************!*\
  !*** ./application/singleton/object_freeze.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst ObjectFreezeDemo = () => {\n  /*\n  \tРазберемся вначале с Object.freeze. ->\n  \thttps://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze\n  */\n  const myObj = {\n    name: 'Dexter',\n    prop: () => {\n      console.log(`${undefined.name} woohoo!`);\n    }\n  };\n  console.log(myObj);\n  myObj.name = \"Debra\";\n  console.log(myObj);\n  /*\n      Заморозка обьекта, это необратимый процесс.\n      Единожды заомроженный обьект уже не может быть разморожен!\n      Заморозим обьект\n  */\n\n  let frozen = Object.freeze(myObj); //Попробуем изменить или добавить значение\n  // myObj.name = 'name';\n  // frozen.name = \"Vince\";\n  // frozen.secondName = 'Morgan';\n  // Проверим сам обьект и его идентичность с тем,\n  // что мы создали в самом начале функции\n  // console.log( 'frozen', frozen, 'equal?', frozen === myObj);\n\n  /*\n  \tТак же, метод работает для массивов\n  */\n\n  let frozenArray = Object.freeze(['froze', 'inside', 'of', 'array']); // Попробуем добавить новое значение\n  // frozenArray[0] = 'Noooooo!';\n  // Попробуем использовать методы\n\n  let sliceOfColdAndSadArray = frozenArray.slice(0, 1);\n  sliceOfColdAndSadArray.map(item => console.log(item));\n  console.log(frozenArray, sliceOfColdAndSadArray); // Метоы для проверки\n  // Object.isFrozen( obj ) -> Вернет true если обьект заморожен\n\n  console.log('is myObj frozen?', Object.isFrozen(myObj), '\\nis frozen frozen?', Object.isFrozen(frozen), '\\nis array frozen', Object.isFrozen(frozenArray));\n  /*\n  \tЗаморозка в обьектах является не глубокой\n  */\n\n  let universe = {\n    infinity: Infinity,\n    good: ['cats', 'love', 'rock-n-roll'],\n    evil: {\n      bonuses: ['cookies', 'great look']\n    }\n  };\n  let frozenUniverse = Object.freeze(universe);\n  frozenUniverse.humans = [];\n  frozenUniverse.evil.humans = [];\n  console.log(frozenUniverse);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObjectFreezeDemo);\n\n//# sourceURL=webpack:///./application/singleton/object_freeze.js?");

/***/ }),

/***/ "./application/singleton/old-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/old-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n\tIIFE - Immediately Invoked Function Expression\n\t( function(){\n\n\t})();\n*/\nconst OldSingleton = function () {\n  var _data = [];\n\n  function add(item) {\n    _data.push(item);\n  }\n\n  function showData() {\n    return [..._data];\n  }\n\n  function get(id) {\n    return _data.find(d => {\n      return d.id === id;\n    });\n  }\n\n  return {\n    add: add,\n    get: get,\n    showData: showData\n  };\n}();\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (OldSingleton);\n\n//# sourceURL=webpack:///./application/singleton/old-singleton.js?");

/***/ }),

/***/ "./application/singleton/oop-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/oop-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass Single {\n  constructor(name) {\n    if (Single.instance === null) {\n      this.name = name;\n      this.data = [];\n      Single.instance = this;\n    }\n\n    return Single.instance;\n  }\n\n  addToData() {\n    this.data.push(1);\n  }\n\n}\n\nSingle.instance = null;\n/* harmony default export */ __webpack_exports__[\"default\"] = (Single);\n\n//# sourceURL=webpack:///./application/singleton/oop-singleton.js?");

/***/ }),

/***/ "./classworks/composition.js":
/*!***********************************!*\
  !*** ./classworks/composition.js ***!
  \***********************************/
/*! exports provided: default, HeadHunt */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"HeadHunt\", function() { return HeadHunt; });\nconst Composition = () => {\n  const makeBackEndMagic = state => ({\n    makeBackEndMagic: arg => console.log(`${state.name} Makes Back End Magic ${arg ? 'and ' + arg : ''}`)\n  });\n\n  const makeFrontEndMagic = state => ({\n    makeFrontEndMagic: () => console.log(`${state.name} Makes Front End Magic`)\n  });\n\n  const MakeItLooksBeautiful = state => ({\n    MakeItLooksBeautiful: () => console.log(`${state.name} Makes It Looks Beautiful`)\n  });\n\n  const DistributeTasks = state => ({\n    DistributeTasks: () => console.log(`${state.name} Distributes Tasks`)\n  });\n\n  const DrinkSomeTea = state => ({\n    DrinkSomeTea: () => console.log(`${state.name} Drinks Some Tea`)\n  });\n\n  const WatchYoutube = state => ({\n    WatchYoutube: () => console.log(`${state.name} Watches Youtube`)\n  });\n\n  const Procrastinate = state => ({\n    Procrastinate: () => console.log(`${state.name} Procrastinates`)\n  });\n\n  class Employee {\n    constructor(name, gender, age) {\n      this.name = name;\n      this.gender = gender;\n      this.age = age;\n    }\n\n  }\n\n  class BackEndDeveleper extends Employee {\n    constructor(name, gender, age) {\n      super(name, gender, age);\n      this.type = 'backend';\n      return Object.assign(this, makeBackEndMagic(this), DrinkSomeTea(this), Procrastinate(this));\n    }\n\n  }\n\n  class FrontEndDeveleper extends Employee {\n    constructor(name, gender, age) {\n      super(name, gender, age);\n      this.type = 'frontend';\n      return Object.assign(this, makeFrontEndMagic(this), DrinkSomeTea(this), WatchYoutube(this));\n    }\n\n  }\n\n  class Designer extends Employee {\n    constructor(name, gender, age) {\n      super(name, gender, age);\n      this.type = 'design';\n      return Object.assign(this, MakeItLooksBeautiful(this), WatchYoutube(this), Procrastinate(this));\n    }\n\n  }\n\n  class ProjectManager extends Employee {\n    constructor(name, gender, age) {\n      super(name, gender, age);\n      this.type = 'project';\n      return Object.assign(this, DistributeTasks(this), Procrastinate(this), DrinkSomeTea(this));\n    }\n\n  }\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Composition);\nclass HeadHunt {\n  createTable() {\n    let table = document.createElement('table');\n    table.classList.add('js-head-hunt-table');\n    document.body.appendChild(table);\n  }\n\n  hire(element) {\n    const newEmployee = {\n      backend: new BackEndDeveleper(element.name, element.gender, element.age),\n      frontend: new FrontEndDeveleper(element.name, element.gender, element.age),\n      design: new Designer(element.name, element.gender, element.age),\n      project: new ProjectManager(element.name, element.gender, element.age)\n    };\n\n    if (element.type) {\n      return newEmployee[element.type];\n    } else {\n      return;\n    }\n  }\n\n  render(element) {\n    let node = document.createElement('tr');\n    let table = document.querySelector('.js-head-hunt-table');\n    node.innerHTML = `<td><b>Name:</b> ${element.name}</td><td><b>Age:</b> ${element.age}</td><td><b>Gender:</b> ${element.gender}</td><td><b>Type:</b> ${element.type}</td>`;\n    table.appendChild(node);\n  }\n\n}\n\n//# sourceURL=webpack:///./classworks/composition.js?");

/***/ }),

/***/ "./classworks/objectfreeze.js":
/*!************************************!*\
  !*** ./classworks/objectfreeze.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание: написать функцию, для глубокой заморозки обьектов.\n\n  Обьект для работы:\n  \n\n  frozenUniverse.evil.humans = []; -> Не должен отработать.\n\n  Методы для работы:\n  1. Object.getOwnPropertyNames(obj);\n      -> Получаем имена свойств из объекта obj в виде массива\n\n  2. Проверка через typeof на обьект или !== null\n  if (typeof prop == 'object' && prop !== null){...}\n\n  Тестирование:\n\n  let FarGalaxy = DeepFreeze(universe);\n      FarGalaxy.good.push('javascript'); // false\n      FarGalaxy.something = 'Wow!'; // false\n      FarGalaxy.evil.humans = [];   // false\n\n*/\nlet universe = {\n  infinity: Infinity,\n  good: ['cats', 'love', 'rock-n-roll'],\n  evil: {\n    bonuses: ['cookies', 'great look'],\n    bonuses2: {\n      test: {\n        all: 1\n      }\n    }\n  }\n};\n\nconst DeepFreeze = obj => {\n  let frozen = Object.freeze(obj);\n  let keys = Object.getOwnPropertyNames(obj);\n  console.log('keys', keys);\n  keys.forEach(key => {\n    if (typeof obj[key] == 'object' && obj[key] !== null) {\n      DeepFreeze(obj[key]);\n    }\n  });\n  return frozen;\n};\n\nconst objectfreeze = () => {\n  let FarGalaxy = DeepFreeze(universe); // FarGalaxy.good.push('javascript'); // false\n  // FarGalaxy.something = 'Wow!'; // false\n  // FarGalaxy.evil.humans = [];   // false\n  // universe.good = 'no';\n\n  universe.evil.bonuses2.test = 11; // console.log(universe);\n}; // objectfreeze();\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (objectfreeze);\n\n//# sourceURL=webpack:///./classworks/objectfreeze.js?");

/***/ }),

/***/ "./classworks/singleton.js":
/*!*********************************!*\
  !*** ./classworks/singleton.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание:\n\n    Написать синглтон, который будет создавать обьект government\n\n    Данные:\n    {\n        laws: [\n        {\n          id: 0,\n          text: '123123'\n        }\n      ],\n        budget: 1000000\n        citizensSatisfactions: 0,\n    }\n\n    У этого обьекта будут методы:\n      .добавитьЗакон({id, name, description})\n        -> добавляет закон в laws и понижает удовлетворенность граждан на -10\n\n      .читатькКонституцию -> Вывести все законы на экран\n      .читатьЗакон(ид)\n\n      .показатьУровеньДовольства()\n      .показатьБюджет()\n      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5\n\n*/\nconst _data = {\n  laws: [{\n    id: 0,\n    text: 'abcdef'\n  }],\n  budget: 1000000,\n  citizensSatisfactions: 0\n};\nconst government = {\n  add: item => {\n    _data.laws.push(item);\n\n    _data.citizensSatisfactions += -10;\n    console.log('New Law:', item);\n  },\n  reedAll: () => {\n    _data.laws.forEach(law => {\n      console.log('Law:', law);\n    });\n  },\n  reed: id => {\n    console.log(`Law ID #${id}:`, _data.laws[id].text);\n  },\n  showLevel: () => {\n    console.log('Satisfaction:', _data.citizensSatisfactions);\n  },\n  celebrate: () => {\n    _data.citizensSatisfactions += 5;\n    _data.budget += -5000;\n    console.log('Satisfaction:', _data.citizensSatisfaction, '| Budget:', _data.budget);\n  }\n};\nObject.freeze(government);\n\nconst singleton = () => {\n  government.add({\n    id: 1,\n    text: 'qwerty'\n  });\n  government.add({\n    id: 2,\n    text: 'zxcvbn'\n  });\n  government.add({\n    id: 3,\n    text: 'poiuyt'\n  });\n  government.reedAll();\n  government.reed(3);\n  government.showLevel();\n  government.celebrate();\n  government.celebrate();\n  government.celebrate(); // government.meeting = 'Do Nothing'\n\n  console.log(government);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (singleton);\n\n//# sourceURL=webpack:///./classworks/singleton.js?");

/***/ })

/******/ });