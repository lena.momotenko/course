export class Action {
    constructor( action ){
        this.action = action;
    }
}

export const ACTION_LIMIT = 555;

export default Action;