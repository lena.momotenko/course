export class User {
    constructor( name ){
        this.id = new Date().getTime();
        this.name = name;
    }
}

export const USER_LIMIT = 15;

export default User;