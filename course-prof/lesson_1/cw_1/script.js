const data = [
  {
    _id: 1,
    link: "#1",
    name: "Established fact",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
  },
  {
    _id: 2,
    link: "#2",
    name: "Many packages",
    description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400859_file_162309.jpg",
  },
  {
    _id: 3,
    link: "#3",
    name: "Suffered alteration",
    description: "Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature.",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400896_file_162315.jpg",
  },
  {
    _id: 4,
    link: "#4",
    name: "Discovered source",
    description: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400878_file_162324.jpg",
  },
  {
    _id: 5,
    link: "#5",
    name: "Handful model",
    description: "The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400876_file_162328.jpg",
  },
];

const dataLocal = {
  like: {},
  comment: {}
};

class Post {
  constructor ({ _id, link, name, description, image, like = 0, comment = '' }) {
    this._id = _id;
    this.link = link;
    this.name = name;
    this.description = description;
    this.image = image;
    this.like = like;
    this.comment = comment;

    this.addComment = this.addComment.bind(this);
    this.addLike = this.addLike.bind(this);
  }

  addLike (e) {
    if(dataLocal.like[this._id] !== undefined) {
      return
    }
    this.like += 1;
    e.target.value = this.like;
    dataLocal.like[this._id] = this.like;
  }

  addComment (e) {
    this.comment += 1;
    e.target.value = this.comment;
    dataLocal.comment[this._id] = this.comment;
    console.log(dataLocal);
  }
  
  render() {
    const root = document.querySelector('.result');
    const newPostHTML = `
      <img src="${this.image}" class="img">
      <div class="info" id="${this._id}">
        <h2 class="name"><a href="${this.link}" class="link">${this.name}</a></h2>
        <p class="description">${this.description}</p>
        <input class="like" type="button" value="${this.like}">
        <label class="comment">
          <input class="comment-field" type="input" value="${this.comment}">
          <input class="comment-btn" type="button" value="Add"> 
        </label>
      </div>
    `;
    const newPost = document.createElement('div');
    newPost.classList.add('post');
    newPost.setAttribute('id', this._id)
    newPost.innerHTML = newPostHTML;
    const likeBtn = newPost.querySelector('.like');
    const comment = newPost.querySelector('.comment');
    likeBtn.addEventListener('click', this.addLike);
    comment.addEventListener('click', this.addComment);
    root.appendChild(newPost);
  }

  init() {
    this.render();
  }
}

document.addEventListener('DOMContentLoaded', () => {
  data.forEach(function(item) {
    let post = new Post(item);
    post.init();
  });
});