import Zoo from '../homework/zoo/zoo';
import { Mammal, Fish, Reptile, Bird } from '../homework/zoo/animal';

document.addEventListener('DOMContentLoaded', () => {
  console.log('zoo');
  let zoo = new Zoo('Zoopark');
  console.log(zoo);
  zoo.countAnimals();
  
  let dog = new Mammal('Bobik', 'mammals', 'Gafff', 'carnivore', 1, 40);
  let cat = new Mammal('Salem', 'mammals', 'Gafff', 'carnivore', 1, 40);
  dog.run();
  let seledka = new Fish('Kisa', 'fishes', 'Bul-bul', 'carnivore', 0.1, 20);
  seledka.swim();
  
  zoo.addAnimal(dog);
  zoo.addAnimal(seledka);
  zoo.addAnimal(cat);
  
  zoo.removeAnimal(cat, 'Salem');
  
  zoo.getAnimal(seledka);
  
  zoo.countAnimals();
  zoo.showList();
});



