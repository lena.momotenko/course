/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          + Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным

    <div class="post">
      <div class="post__title">Some post</div>
      <div class="post__image">
        <img src="https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png"/>
      </div>
      <div class="post__description"></div>
      <div class="post__footer">
        <button class="post__like">Like!</button>
      </div>
    </div>
*/

const data = [
  {
    _id: 0,
    title: "Маска-желе",
    description: "Маска-желе для защиты и сохранения цвета окрашенных волос - L'Oreal Professionnel Vitamino Color A-OX Mask",
    image: "https://u.makeup.com.ua/q/qb/qbso119opgyd.jpg",
  },
  {
    _id: 1,
    title: "Established",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
  },
  {
    _id: 2,
    title: "Established",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
  },
  {
    _id: 3,
    title: "Established",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
  },
  {
    _id: 4,
    title: "Established",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
  },
  {
    _id: 5,
    title: "Established",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
  },
  {
    _id: 6,
    title: "Established",
    description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
    image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg",
  },
];

class Post {
  constructor (postId, title, description, image, like = 0) {
    this._id = postId;
    this._title = title;
    this._description = description;
    this._image = image;
    this._like = like;
  }

  get title() {
    return this._title;
  }

  set title(newTitle) {
    if (newTitle.length > 10) {
      this._title = newTitle;
    }
  }

  get description() {
    return this._description;
  }

  set description(newDescription) {
    if (newDescription.length > 10) {
      this._description = newDescription;
    }
  }

  get image() {
    return this._image;
  }

  set image(newImage) {
    if (newImage.length > 10) {
      this._image = newImage;
    }
  }

  addLike (target) {
    target.addEventListener('click', () => {
      this._like += 1;
        target.value = this._like;
    });
  }

  render( target = document.body) {
    const newPost = document.createElement('div');
    newPost.classList.add('post');
    newPost.setAttribute('id', this._id);

    newPost.innerHTML = `
      <img src="${this._image}" class="img">
      <div class="info" id="${this._id}">
        <h2 class="name">${this._title}</h2>
        <p class="description">${this._description}</p>
        <input class="like" type="button" value="${this._like}">
      </div>
    `;
    
    const likeBtn = newPost.querySelector('.like');
    this.addLike(likeBtn);
    
    target.appendChild(newPost);
  }

  init() {
    this.render();
  }
}

class Advertisment extends Post {
  buyItem(target) {
    target.addEventListener('click', () => {
      console.log('You, my dear friend, buy this item');
    });
  }

  render( target = document.body) {

    const newPost = document.createElement('div');
    newPost.style.backgroundColor = 'yellow';
    newPost.classList.add('post');
    newPost.setAttribute('id', this._id);

    newPost.innerHTML = `
      <img src="${this._image}" class="img">
      <div class="info" id="${this._id}">
        <h2 class="name">${this._title}</h2>
        <p class="description">${this._description}</p>
        <input class="like" type="button" value="${this._like}">
        <input class="_buyItem" type="button" value="Buy Item">
      </div>
    `;

    const likeBtn = newPost.querySelector('.like');
    const buyBtn = newPost.querySelector('._buyItem');
    this.addLike(likeBtn);
    this.buyItem(buyBtn);
    
    target.appendChild(newPost);
  }
}

const classes = () => {
  data.forEach(function(item, index) {

    let isThird = index % 2 !== 0;
    let notZero = index !== 0;

    let rules = [
      isThird,
      notZero
    ];

    if ( rules.every( i => i === true ) ) {
      let post = new Post(data[index]._id, data[index].title, data[index].description, data[index].image);
      post.init();
      let advertisment = new Advertisment(data[index]._id, data[index].title, data[index].description, data[index].image);
      advertisment.init();
    } else {
      let post = new Post(data[index]._id, data[index].title, data[index].description, data[index].image);
      post.init();
    }
  });
};


export default classes;