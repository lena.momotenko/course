class Zoo {
  constructor (name) {
    this.name = name;
    this.animalCount = 0;
    this.zones = {
      mammals: [],
      birds: [],
      fishes: [],
      reptilies: [],
      others: []
    };
  }

  addAnimal (animalObj) {
    this.zones[animalObj.type].push(animalObj);
    this.animalCount++;
  }

  removeAnimal (animalObj, animalName) {
    [...this.zones[animalObj.type]].forEach(element => {
      if (element.name === animalName) {
        let index = this.zones[animalObj.type].indexOf(element);
        if (index !== -1) this.zones[animalObj.type].splice(index, 1);
        this.animalCount--;
      }
    });
  }

  getAnimal (animal) {
    console.log('animal:', animal);
    animal.init();
  }
  
  countAnimals () {
    console.log(`${this.name} contains: ${this.animalCount} anamal${this.animalCount > 0 ? 's' : ''} `)
  }

  showList () {
    console.group();
    console.log(`%cIn ${this.name} you can find:`, 'color: yellow; font-style: italic; background-color: blue;padding: 2px');
    let zones = Object.getOwnPropertyNames(this.zones);
    zones.forEach((zone) => {
      this.zones[zone].forEach((kind) => {
        console.log(kind);
      });
    });
    console.groupEnd();
  }
}

export default Zoo;