class Animal {
  constructor(name, type = 'others', phrase, foodType, minSpeed, maxSpeed) {
    this.name = name;
    this.type = type;
    this.phrase = phrase;
    this.foodType = foodType;
    this.minSpeed = minSpeed;
    this.maxSpeed = maxSpeed;
    this.speed = (minSpeed + maxSpeed) / 2;
  }

  eatSomething () {
    if (foodType === 'herbivore') {
      console.log(`${this.name} eats grass`);
    } else {
      console.log(`${this.name} eats meat`);
    }
  }
}

class Mammal extends Animal {
  constructor(name, phrase, foodType, minSpeed, maxSpeed, speed) {
    super (name, phrase, foodType, minSpeed, maxSpeed, speed)
  }

  run () {
    console.log(`${this.name} runs with speed ${this.speed} km/h`);
  }

  init() {
    this.run();
  }
}

class Fish extends Animal {
  constructor(name, phrase, foodType, minSpeed, maxSpeed, speed) {
    super (name, phrase, foodType, minSpeed, maxSpeed, speed)
  }

  swim () {
    console.log(`${this.name} swims with speed ${this.speed} km/h`);
  }

  init() {
    this.swim();
  }
}

class Reptile extends Animal {
  constructor(name, phrase, foodType, minSpeed, maxSpeed, speed) {
    super (name, phrase, foodType, minSpeed, maxSpeed, speed)
  }

  creep () {
    console.log(`${this.name} creeps with speed ${this.speed} km/h`);
  }

  init() {
    this.creep();
  }
}

class Bird extends Animal {
  constructor(name, phrase, foodType, minSpeed, maxSpeed, speed) {
    super (name, phrase, foodType, minSpeed, maxSpeed, speed)
  }

  fly () {
    console.log(`${this.name} flies with speed ${this.speed} km/h`);
  }

  init() {
    this.fly();
  }
}

export { Mammal, Fish, Reptile, Bird };