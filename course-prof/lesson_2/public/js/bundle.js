/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _homework_zoo_zoo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../homework/zoo/zoo */ \"./homework/zoo/zoo.js\");\n/* harmony import */ var _homework_zoo_animal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../homework/zoo/animal */ \"./homework/zoo/animal.js\");\n\n\ndocument.addEventListener('DOMContentLoaded', function () {\n  console.log('zoo');\n  var zoo = new _homework_zoo_zoo__WEBPACK_IMPORTED_MODULE_0__[\"default\"]('Zoopark');\n  console.log(zoo);\n  zoo.countAnimals();\n  var dog = new _homework_zoo_animal__WEBPACK_IMPORTED_MODULE_1__[\"Mammal\"]('Bobik', 'mammals', 'Gafff', 'carnivore', 1, 40);\n  var cat = new _homework_zoo_animal__WEBPACK_IMPORTED_MODULE_1__[\"Mammal\"]('Salem', 'mammals', 'Gafff', 'carnivore', 1, 40);\n  var lastochka = new _homework_zoo_animal__WEBPACK_IMPORTED_MODULE_1__[\"Bird\"]('Hope', 'birds', 'Ayyy', 'carnivore', 1, 28);\n  var snake = new _homework_zoo_animal__WEBPACK_IMPORTED_MODULE_1__[\"Reptile\"]('Nagini', 'reptilies', 'Shhhhhhh', 'carnivore', 0.2, 18);\n  dog.run();\n  var seledka = new _homework_zoo_animal__WEBPACK_IMPORTED_MODULE_1__[\"Fish\"]('Kisa', 'fishes', 'Bul-bul', 'carnivore', 0.1, 20);\n  var tuna = new _homework_zoo_animal__WEBPACK_IMPORTED_MODULE_1__[\"Fish\"]('Tom', 'fishes', 'Bul-bul', 'carnivore', 0.1, 20);\n  seledka.swim();\n  zoo.addAnimal(dog);\n  zoo.addAnimal(seledka);\n  zoo.addAnimal(cat);\n  zoo.addAnimal(snake);\n  zoo.addAnimal(tuna);\n  zoo.removeAnimal(cat, 'Salem');\n  zoo.addAnimal(lastochka);\n  zoo.getAnimal(seledka);\n  zoo.countAnimals();\n  zoo.showList();\n});\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./homework/zoo/animal.js":
/*!********************************!*\
  !*** ./homework/zoo/animal.js ***!
  \********************************/
/*! exports provided: Mammal, Fish, Reptile, Bird */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Mammal\", function() { return Mammal; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Fish\", function() { return Fish; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Reptile\", function() { return Reptile; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Bird\", function() { return Bird; });\nfunction _typeof(obj) { if (typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === \"function\" && obj.constructor === Symbol && obj !== Symbol.prototype ? \"symbol\" : typeof obj; }; } return _typeof(obj); }\n\nfunction _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === \"object\" || typeof call === \"function\")) { return call; } return _assertThisInitialized(self); }\n\nfunction _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return self; }\n\nfunction _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function\"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }\n\nfunction _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nvar Animal =\n/*#__PURE__*/\nfunction () {\n  function Animal(name) {\n    var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'others';\n    var phrase = arguments.length > 2 ? arguments[2] : undefined;\n    var foodType = arguments.length > 3 ? arguments[3] : undefined;\n    var minSpeed = arguments.length > 4 ? arguments[4] : undefined;\n    var maxSpeed = arguments.length > 5 ? arguments[5] : undefined;\n\n    _classCallCheck(this, Animal);\n\n    this.name = name;\n    this.type = type;\n    this.phrase = phrase;\n    this.foodType = foodType;\n    this.minSpeed = minSpeed;\n    this.maxSpeed = maxSpeed;\n    this.speed = (minSpeed + maxSpeed) / 2;\n  }\n\n  _createClass(Animal, [{\n    key: \"eatSomething\",\n    value: function eatSomething() {\n      if (foodType === 'herbivore') {\n        console.log(\"\".concat(this.name, \" eats grass\"));\n      } else {\n        console.log(\"\".concat(this.name, \" eats meat\"));\n      }\n    }\n  }]);\n\n  return Animal;\n}();\n\nvar Mammal =\n/*#__PURE__*/\nfunction (_Animal) {\n  _inherits(Mammal, _Animal);\n\n  function Mammal(name, phrase, foodType, minSpeed, maxSpeed, speed) {\n    _classCallCheck(this, Mammal);\n\n    return _possibleConstructorReturn(this, _getPrototypeOf(Mammal).call(this, name, phrase, foodType, minSpeed, maxSpeed, speed));\n  }\n\n  _createClass(Mammal, [{\n    key: \"run\",\n    value: function run() {\n      console.log(\"\".concat(this.name, \" runs with speed \").concat(this.speed, \" km/h\"));\n    }\n  }, {\n    key: \"init\",\n    value: function init() {\n      this.run();\n    }\n  }]);\n\n  return Mammal;\n}(Animal);\n\nvar Fish =\n/*#__PURE__*/\nfunction (_Animal2) {\n  _inherits(Fish, _Animal2);\n\n  function Fish(name, phrase, foodType, minSpeed, maxSpeed, speed) {\n    _classCallCheck(this, Fish);\n\n    return _possibleConstructorReturn(this, _getPrototypeOf(Fish).call(this, name, phrase, foodType, minSpeed, maxSpeed, speed));\n  }\n\n  _createClass(Fish, [{\n    key: \"swim\",\n    value: function swim() {\n      console.log(\"\".concat(this.name, \" swims with speed \").concat(this.speed, \" km/h\"));\n    }\n  }, {\n    key: \"init\",\n    value: function init() {\n      this.swim();\n    }\n  }]);\n\n  return Fish;\n}(Animal);\n\nvar Reptile =\n/*#__PURE__*/\nfunction (_Animal3) {\n  _inherits(Reptile, _Animal3);\n\n  function Reptile(name, phrase, foodType, minSpeed, maxSpeed, speed) {\n    _classCallCheck(this, Reptile);\n\n    return _possibleConstructorReturn(this, _getPrototypeOf(Reptile).call(this, name, phrase, foodType, minSpeed, maxSpeed, speed));\n  }\n\n  _createClass(Reptile, [{\n    key: \"creep\",\n    value: function creep() {\n      console.log(\"\".concat(this.name, \" creeps with speed \").concat(this.speed, \" km/h\"));\n    }\n  }, {\n    key: \"init\",\n    value: function init() {\n      this.creep();\n    }\n  }]);\n\n  return Reptile;\n}(Animal);\n\nvar Bird =\n/*#__PURE__*/\nfunction (_Animal4) {\n  _inherits(Bird, _Animal4);\n\n  function Bird(name, phrase, foodType, minSpeed, maxSpeed, speed) {\n    _classCallCheck(this, Bird);\n\n    return _possibleConstructorReturn(this, _getPrototypeOf(Bird).call(this, name, phrase, foodType, minSpeed, maxSpeed, speed));\n  }\n\n  _createClass(Bird, [{\n    key: \"fly\",\n    value: function fly() {\n      console.log(\"\".concat(this.name, \" flies with speed \").concat(this.speed, \" km/h\"));\n    }\n  }, {\n    key: \"init\",\n    value: function init() {\n      this.fly();\n    }\n  }]);\n\n  return Bird;\n}(Animal);\n\n\n\n//# sourceURL=webpack:///./homework/zoo/animal.js?");

/***/ }),

/***/ "./homework/zoo/zoo.js":
/*!*****************************!*\
  !*** ./homework/zoo/zoo.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }\n\nfunction _nonIterableSpread() { throw new TypeError(\"Invalid attempt to spread non-iterable instance\"); }\n\nfunction _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === \"[object Arguments]\") return Array.from(iter); }\n\nfunction _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nvar Zoo =\n/*#__PURE__*/\nfunction () {\n  function Zoo(name) {\n    _classCallCheck(this, Zoo);\n\n    this.name = name;\n    this.animalCount = 0;\n    this.zones = {\n      mammals: [],\n      birds: [],\n      fishes: [],\n      reptilies: [],\n      others: []\n    };\n  }\n\n  _createClass(Zoo, [{\n    key: \"addAnimal\",\n    value: function addAnimal(animalObj) {\n      this.zones[animalObj.type].push(animalObj);\n      this.animalCount++;\n    }\n  }, {\n    key: \"removeAnimal\",\n    value: function removeAnimal(animalObj, animalName) {\n      var _this = this;\n\n      _toConsumableArray(this.zones[animalObj.type]).forEach(function (element) {\n        if (element.name === animalName) {\n          var index = _this.zones[animalObj.type].indexOf(element);\n\n          if (index !== -1) _this.zones[animalObj.type].splice(index, 1);\n          _this.animalCount--;\n        }\n      });\n    }\n  }, {\n    key: \"getAnimal\",\n    value: function getAnimal(animal) {\n      console.log('animal:', animal);\n      animal.init();\n    }\n  }, {\n    key: \"countAnimals\",\n    value: function countAnimals() {\n      console.log(\"\".concat(this.name, \" contains: \").concat(this.animalCount, \" anamal\").concat(this.animalCount > 0 ? 's' : '', \" \"));\n    }\n  }, {\n    key: \"showList\",\n    value: function showList() {\n      var _this2 = this;\n\n      console.group();\n      console.log(\"%cIn \".concat(this.name, \" you can find:\"), 'color: yellow; font-style: italic; background-color: blue;padding: 2px');\n      var zones = Object.getOwnPropertyNames(this.zones);\n      zones.forEach(function (zone) {\n        _this2.zones[zone].forEach(function (kind) {\n          console.log(kind);\n        });\n      });\n      console.groupEnd();\n    }\n  }]);\n\n  return Zoo;\n}();\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Zoo);\n\n//# sourceURL=webpack:///./homework/zoo/zoo.js?");

/***/ })

/******/ });