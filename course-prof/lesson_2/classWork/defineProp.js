
/*

    Задание, написать класс, который создает объекты по модельке:
    Dog {
        name: "", -> not configurable
        breed: "", -> not configurable, not editable
        weight: "",
        isGoodBoy: true -> enumerable 
    }   

*/

class Animal {
    constructor (name, breed, weight, isGoodBoy = true) {
        Object.defineProperty(this, '_name', { 
            configurable: true,
            value: name
        });
        Object.defineProperty(this, '_breed', { 
            configurable: false,
            writable: true,
            value: breed
        });
        this._weight = weight;
        Object.defineProperty(this, '_isGoodBoy', { 
            enumerable: true,
            value: isGoodBoy
        });
    }
}

document.addEventListener('DOMContentLoaded', function() {
    let dog = new Animal('Doctor', 'ovcharka', 80, false);
    dog._breed = 'taksa';
    console.log(dog);
});