import { WeaponFactory, Weapon } from './weaponFactory';



const SuperFactory = ( type ) => {
    // Наша абстрактная фабрика 
    return type === "modern" ? ModernWeaponFactory : WeaponFactory;
}

class Gun extends Weapon {
    constructor({name = 'Unnamed gun', material = 'steel', magic}){
        super();
        this.weaponType = 'gun';
        this.name = name;
        this.material = material;
        this.magic = magic !== undefined ? magic : false;
        this.icon = 'images/2053053.png';
    }
}


class ModernWeaponFactory {
    makeWeapon( weapon ){
        let WeaponClass = null;
        if( weapon.weaponType === 'gun'){
            WeaponClass = Gun;
        } else {
            return false;
        }
        return new WeaponClass( weapon );
    }



}

const AbsFactory = () => {

    const Abs = SuperFactory('modern');
    const fabric = new Abs();

    console.log( fabric ); 

    let gun = fabric.makeWeapon({
        weaponType: 'gun',
        name: 'defender',
        material: 'steel',
        magic: false
    });

    gun.render();


}


export default AbsFactory;