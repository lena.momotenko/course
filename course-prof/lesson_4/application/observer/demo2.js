import { Observable, Observer } from './Observer';
import Products from './products.json';


const Demo2 = () => {
	console.log('DEMO 2');
	/*
		Рассмотрим на примере простого интернет магазина.
		Создадим наблюдателя:
	*/
	let observable = new Observable();
	
	/*
		Три обсервера на взаимодействие с: Модулем корзины / Серверной обработкой / Интерфесом
	*/

	let cartObs = new Observer( function(id){
		let filtredToBasket = Products.find( item => Number(item.id) === Number(id) );
			Cart.push( filtredToBasket );
			renderBasket();
	});

	let serverObs = new Observer( (id) => {
		let filtredToBasket = Products.find( item => Number(item.id) === Number(id) );
		console.log( `Товар ${filtredToBasket.name} добавлен в корзину пользователя на сервере` );
	});

	let iconObs = new Observer( (id) => {
		let products__cart = document.getElementById('products__cart');
			products__cart.innerText = Cart.length;
	});

	observable.addObserver( cartObs );
	observable.addObserver( serverObs );
	observable.addObserver( iconObs );


  // Render Data - - - - - - - - - - - -
	let Cart = [];
	let products__row = document.getElementById('products__row');

	const renderBasket = () => {
		let cartElem = document.getElementById('cart');
		let message;

			if( Cart.length === 0 ){
				message = 'У вас в корзине пусто';
			} else {
			let Sum = Cart.reduce( (prev, current) => {
				return prev += Number(current.price);
			}, 0);
				message = `У вас в корзине ${Cart.length} товаров, на сумму: ${Sum} грн.`;
			}

			cartElem.innerHTML = `<h2>${message}</h2><ol></ol>`;

			let ol = cartElem.querySelector('ol');
			Cart.map( item => {
			let li = document.createElement('li');
				li.innerText = `${item.name} (${item.price} грн.)`;
				ol.appendChild(li);
			});
	}

	Products.map( item => {
		let product = document.createElement('div');
			product.className = "product";
			product.innerHTML =
			`<div class="product__image">
				<img src="${item.imageLink}"/>
			</div>
			<div class="product__name">${item.name}</div>
			<div class="product__price">${item.price} грн.</div>
			<div class="product__action">
				<button class="product__buy" data-id=${item.id}> Купить </button>
			</div>`;
			let buyButton = product.querySelector('.product__buy');
				buyButton.addEventListener('click', (e) => {
				let id = e.target.dataset.id;
				observable.sendMessage( id );
			});
			products__row.appendChild(product);
	});

	renderBasket();
};

export default Demo2;
