/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _trafficLight__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./trafficLight */ \"./application/trafficLight.js\");\n// import factory from './factory';\n// import observer from './observer';\n// import CustomEvents from './CustomEvents';\n // factory();\n// observer();\n// CustomEvents();\n\nObject(_trafficLight__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/trafficLight.js":
/*!*************************************!*\
  !*** ./application/trafficLight.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n    Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в\n                соответсвии с правилавми ниже:\n\n    1. Написать кастомные события которые будут менять статус светофора:\n    - start: включает зеленый свет\n    - stop: включает красный свет\n    - night: включает желтый свет, который моргает с интервалом в 1с.\n    И зарегистрировать каждое через addEventListener на каждом из светофоров.\n\n    2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,\n        чтобы включить режим \"нерегулируемого перекрестка\" (моргающий желтый).\n\n    3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)\n        или зеленый (на второй клик) цвет соотвественно.\n        Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.\n\n    4.  + Бонус: На кнопку \"Start Night\" повесить сброс всех светофоров с их текущего\n        статуса, на мигающий желтые.\n        Двойной, тройной и более клики на кнопку не должны вызывать повторную\n        инициализацию инвервала.\n*/\nfunction trafficLight() {\n  let btn = document.getElementById('Do');\n  const wrap = document.getElementById('app');\n  let trafficLights = document.querySelectorAll('.trafficLight');\n  trafficLights.forEach(element => {\n    const lights = ['green', 'yellow', 'red'];\n    let i = 0;\n    element.addEventListener('click', () => {\n      if (i == lights.length) {\n        i = 0;\n        element.classList.remove(lights[lights.length - 1]);\n      } else {\n        element.classList.remove(lights[i - 1]);\n      }\n\n      element.classList.add(lights[i]);\n      i++;\n      let number = element.getAttribute('id');\n      console.log(number); // let trafficLightEvent = new CustomEvent('trafficLightChange', {\n      //   detail: {\n      //     number: number,\n      //   }\n      // });\n      // wrap.dispatchEvent(trafficLightEvent);\n    });\n  });\n  console.log(lights);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (trafficLight);\n\n//# sourceURL=webpack:///./application/trafficLight.js?");

/***/ })

/******/ });