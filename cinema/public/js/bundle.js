/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./client/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./client/index.js":
/*!*************************!*\
  !*** ./client/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./routes */ \"./client/routes.js\");\n/* harmony import */ var _script_generateCinemaHall__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./script/generateCinemaHall */ \"./client/script/generateCinemaHall.js\");\n/* harmony import */ var _script_buyTicket__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./script/buyTicket */ \"./client/script/buyTicket.js\");\n\n\n\nconst GenerateCinemaHall = new _script_generateCinemaHall__WEBPACK_IMPORTED_MODULE_1__[\"default\"](\"Cinema2\", 11, 11);\nlet sheme = GenerateCinemaHall.init();\nconst saveBtn = document.querySelector('.js-save-btn');\nObject(_script_buyTicket__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(sheme); // let customizeBlock = document.querySelector('.js-customize');\n// console.log(row);\n// if (flag) {\n// \tcustomizeBlock.classList.remove('hidden');\n// \trow.classList.add('focus');\n// \tflag = false;\n// } else {\n// \tcustomizeBlock.classList.add('hidden');\n// \trow.classList.remove('focus');\n// \tflag = true;\n// }\n// const addBtn = document.querySelector('.js-add-place');\n// addBtn.addEventListener('click', () => {\n// \tgenerateCinemaHall.changePlace(row)\n// });\n// customizeCinemaHall();\n\nconst initRouter = () => {\n  const key = window.location.hash.replace('#', '');\n  console.log(key);\n  const route = _routes__WEBPACK_IMPORTED_MODULE_0__[\"default\"].find(item => item.path === key);\n  renderRoute(route);\n};\n\nconst renderRoute = route => {\n  const root = document.getElementById('root');\n  root.innerHTML = null; // root.appendChild( route.component()  );\n};\n\nconst pushToHistory = ({\n  title,\n  path\n}) => e => {\n  const hashedUrl = `${window.location.origin}/#${path}`;\n  const key = window.location.hash.replace('#', '');\n  const route = _routes__WEBPACK_IMPORTED_MODULE_0__[\"default\"].find(item => item.path === key);\n  window.history.pushState(null, title, hashedUrl);\n  renderRoute(route);\n};\n\nconst renderNaviation = () => {\n  let nav = document.createElement('nav');\n  _routes__WEBPACK_IMPORTED_MODULE_0__[\"default\"].map(route => {\n    let btn = document.createElement('button');\n    btn.textContent = route.title;\n    btn.addEventListener('click', pushToHistory(route));\n    nav.appendChild(btn);\n  });\n  document.body.appendChild(nav);\n};\n\ndocument.addEventListener('DOMContentLoaded', () => {\n  renderNaviation();\n  initRouter();\n  window.addEventListener('popstate', e => {\n    console.log('history change', e);\n    initRouter();\n  });\n});\n\n//# sourceURL=webpack:///./client/index.js?");

/***/ }),

/***/ "./client/pages/cinema.js":
/*!********************************!*\
  !*** ./client/pages/cinema.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst cinema = () => {\n  const cinema = document.createElement('section');\n  cinema.innerHTML = `\n        <h1>Single</h1>\n    `;\n  return cinema;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (cinema);\n\n//# sourceURL=webpack:///./client/pages/cinema.js?");

/***/ }),

/***/ "./client/routes.js":
/*!**************************!*\
  !*** ./client/routes.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _pages_cinema__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages/cinema */ \"./client/pages/cinema.js\");\n\nconst ROUTES = [{\n  path: '/',\n  component: _pages_cinema__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  title: 'Cinema'\n}];\n/* harmony default export */ __webpack_exports__[\"default\"] = (ROUTES);\n\n//# sourceURL=webpack:///./client/routes.js?");

/***/ }),

/***/ "./client/script/buyTicket.js":
/*!************************************!*\
  !*** ./client/script/buyTicket.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// import customizeCinemaHall from './customizeCinemaHall';\nconst buyTicket = sheme => {\n  const allPlaces = document.querySelectorAll('.place');\n  const ticketsInfo = document.querySelector('.js-tickets-info');\n  const btnBuy = document.querySelector('.js-buy');\n  let bookedTickets = {};\n  let timer = null;\n\n  const setTimer = () => {\n    return setTimeout(() => {\n      for (let propt in bookedTickets) {\n        let y = bookedTickets[propt].y;\n        let x = bookedTickets[propt].x;\n        let row = document.querySelector(`.row[data-row=\"${y}\"]`);\n        let place = row.querySelector(`.place[data-place=\"${x}\"]`);\n        place.classList.remove('active');\n        delete bookedTickets[`${y}_${x}`];\n      }\n    }, 1000);\n  };\n\n  allPlaces.forEach(place => {\n    place.addEventListener('click', () => {\n      place.classList.toggle('active');\n      let x = place.dataset.place;\n      let y = place.parentNode.dataset.row;\n\n      if (place.classList.contains('active')) {\n        bookedTickets[`${y}_${x}`] = {\n          y,\n          x\n        };\n        sheme[y][x].status = 'booked';\n        clearTimeout(timer);\n        timer = setTimer();\n      } else {\n        delete bookedTickets[`${y}_${x}`];\n        sheme[y][x].status = 'free';\n      }\n\n      ticketsInfo.textContent = bookedTickets.length;\n    });\n  });\n  btnBuy.addEventListener('click', () => {\n    clearTimeout(timer);\n\n    for (let propt in bookedTickets) {\n      let y = bookedTickets[propt].y;\n      let x = bookedTickets[propt].x;\n      sheme[y][x].status = 'bought';\n      let row = document.querySelector(`.row[data-row=\"${y}\"]`);\n      let place = row.querySelector(`.place[data-place=\"${x}\"]`);\n      place.classList.toggle('active');\n      place.classList.add('bought');\n      delete bookedTickets[`${y}_${x}`];\n    }\n\n    console.log(sheme);\n  });\n  return sheme;\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (buyTicket);\n\n//# sourceURL=webpack:///./client/script/buyTicket.js?");

/***/ }),

/***/ "./client/script/generateCinemaHall.js":
/*!*********************************************!*\
  !*** ./client/script/generateCinemaHall.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _buyTicket__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./buyTicket */ \"./client/script/buyTicket.js\");\n// import customizeCinemaHall from './customizeCinemaHall';\n\n\nclass generateCinemaHall {\n  constructor(name = 'New Cinema', rows = 10, places = 10, sheme) {\n    this.name = name;\n    this.sheme = sheme || [];\n    this.rows = rows;\n    this.places = places;\n    this.sheme = sheme;\n  }\n\n  generateHall() {\n    const root = document.createElement('div');\n    root.classList.add('root');\n\n    for (let y = 1; y <= this.rows; y++) {\n      this.generateRow(root, y);\n    } // buyTicket();\n\n\n    document.body.append(root);\n  }\n\n  generateRow(root, y) {\n    const row = document.createElement('div');\n    row.dataset.row = y;\n    row.classList.add('row');\n\n    if (sheme) {\n      console.log(sheme); // for (let x = 1; x <= sheme.length - 1; x++) {\n      //   this.generatePlace(row, x, y);\n      // }\n    } else {\n      this.sheme[y] = [];\n\n      for (let x = 1; x <= this.places; x++) {\n        this.generatePlace(row, x, y);\n      }\n    }\n\n    root.append(row);\n    return row;\n  }\n\n  generatePlace(row, x, y, type = 'standard', price = 1) {\n    const place = document.createElement('div');\n    place.dataset.place = x;\n    place.classList.add('place');\n    place.textContent = x;\n    this.sheme[y][x] = {\n      status: 'free',\n      type: type,\n      price: price\n    };\n    row.append(place);\n  }\n\n  deleteRow(row) {\n    row.remove();\n  }\n\n  deletePlace(row, x, y) {\n    if (x == 1) {\n      return;\n    }\n\n    let placeHTML = row.querySelector(`[data-place='${x}']`);\n    this.sheme[y].splice(x, 1);\n    placeHTML.remove();\n  }\n\n  changePlace(row, add = true) {\n    let y = row.dataset.row;\n    let x = row.children.length;\n\n    if (add) {\n      this.generatePlace(row, x + 1, y);\n    } else {\n      this.deletePlace(row, x, y);\n    }\n  }\n\n  init() {\n    this.generateHall();\n    console.log(this.sheme);\n    return this.sheme;\n  }\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (generateCinemaHall);\n\n//# sourceURL=webpack:///./client/script/generateCinemaHall.js?");

/***/ })

/******/ });