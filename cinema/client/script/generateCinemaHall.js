// import customizeCinemaHall from './customizeCinemaHall';
import buyTicket from './buyTicket';

class generateCinemaHall {
  constructor(name = 'New Cinema', rows = 10, places = 10, sheme) {
    this.name = name;
    this.sheme = sheme || [];
    this.rows = rows;
    this.places = places;
    this.sheme = sheme;
  }

  generateHall() {
    const root = document.createElement('div');
    root.classList.add('root');
    
    for (let y = 1; y <= this.rows; y++) {
      this.generateRow(root, y);  
    }
    // buyTicket();
    document.body.append(root);
  }

  generateRow(root, y) {
    const row = document.createElement('div');
    row.dataset.row = y;
    row.classList.add('row');
    if(sheme) {
      console.log(sheme);
      // for (let x = 1; x <= sheme.length - 1; x++) {
      //   this.generatePlace(row, x, y);
      // }
    } else {
      this.sheme[y] = [];
      for (let x = 1; x <= this.places; x++) {
        this.generatePlace(row, x, y);
      }
    }
    root.append(row);
    return row
  }

  generatePlace(row, x, y, type = 'standard', price = 1) {
    const place = document.createElement('div');
    place.dataset.place = x;
    place.classList.add('place');
    place.textContent = x;
    this.sheme[y][x] = { status : 'free', type : type, price : price };
    row.append(place);
  }

  deleteRow(row) {
    row.remove();
  }

  deletePlace(row, x, y) {
    if (x == 1) {
      return
    }
    let placeHTML = row.querySelector(`[data-place='${x}']`);
    this.sheme[y].splice(x, 1);
    placeHTML.remove();
  }

  changePlace (row, add = true) {
    let y = row.dataset.row;
    let x = row.children.length;
    if (add) {
      this.generatePlace(row, x + 1, y);
    } else {
      this.deletePlace(row, x, y);
    }
  }

  init () {
    this.generateHall();
    console.log(this.sheme);
    return this.sheme;
  }
}

export default generateCinemaHall;