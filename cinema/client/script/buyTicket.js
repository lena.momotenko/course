// import customizeCinemaHall from './customizeCinemaHall';

const buyTicket = sheme => {

  const allPlaces = document.querySelectorAll('.place');
  const ticketsInfo = document.querySelector('.js-tickets-info');
  const btnBuy = document.querySelector('.js-buy');
  let bookedTickets = {};
  let timer = null;

  const setTimer = () => {
    return setTimeout(() => {
      for (let propt in bookedTickets) {
        let y = bookedTickets[propt].y;
        let x = bookedTickets[propt].x;
        let row =  document.querySelector(`.row[data-row="${y}"]`);
        let place = row.querySelector(`.place[data-place="${x}"]`);
        place.classList.remove('active');
        delete bookedTickets[`${y}_${x}`];
      }
    }, 1000);
  }

  allPlaces.forEach(place => {
		place.addEventListener('click', () => {
      place.classList.toggle('active');
      let x = place.dataset.place;
      let y = place.parentNode.dataset.row;
      if (place.classList.contains('active')) {
        bookedTickets[`${y}_${x}`] = {y, x};
        sheme[y][x].status = 'booked';
        clearTimeout(timer);
        timer = setTimer();
      } else {
        delete bookedTickets[`${y}_${x}`];
        sheme[y][x].status = 'free';
      }
      ticketsInfo.textContent = bookedTickets.length;
		});
  });

  btnBuy.addEventListener('click', () => {
    clearTimeout(timer);
    for (let propt in bookedTickets) {
      let y = bookedTickets[propt].y;
      let x = bookedTickets[propt].x;
      sheme[y][x].status = 'bought';
      let row = document.querySelector(`.row[data-row="${y}"]`);
      let place = row.querySelector(`.place[data-place="${x}"]`);
      place.classList.toggle('active');
      place.classList.add('bought');
      delete bookedTickets[`${y}_${x}`];
    }
    console.log(sheme);
  });

  return sheme
}

export default buyTicket;