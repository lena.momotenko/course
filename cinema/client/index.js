import ROUTES from './routes';
import generateCinemaHall from './script/generateCinemaHall';
import buyTicket from './script/buyTicket';


const GenerateCinemaHall = new generateCinemaHall("Cinema2", 11, 11);
let sheme = GenerateCinemaHall.init();

const saveBtn = document.querySelector('.js-save-btn');

buyTicket(sheme);




// let customizeBlock = document.querySelector('.js-customize');
// console.log(row);
// if (flag) {
// 	customizeBlock.classList.remove('hidden');
// 	row.classList.add('focus');
// 	flag = false;
// } else {
// 	customizeBlock.classList.add('hidden');
// 	row.classList.remove('focus');
// 	flag = true;
// }
// const addBtn = document.querySelector('.js-add-place');
// addBtn.addEventListener('click', () => {
// 	generateCinemaHall.changePlace(row)
// });

// customizeCinemaHall();

const initRouter = () => {

    const key =  window.location.hash.replace('#', '');
    console.log( key );
    const route = ROUTES.find( item => item.path === key);

    renderRoute( route );
}

const renderRoute = ( route ) => {

    const root = document.getElementById('root');
    root.innerHTML = null;

    // root.appendChild( route.component()  );
}


const pushToHistory = ({ title, path }) => (e) => {

    const hashedUrl = `${window.location.origin}/#${path}`;
    const key =  window.location.hash.replace('#', '');
    const route = ROUTES.find( item => item.path === key);

    window.history.pushState( null, title, hashedUrl );
    renderRoute( route );

}


const renderNaviation = () => {
    let nav = document.createElement('nav');

    ROUTES.map( route => {

        let btn = document.createElement('button');
            btn.textContent = route.title; 

            btn.addEventListener('click', pushToHistory(route) );
        nav.appendChild(btn);

    })

    document.body.appendChild(nav);
}


document.addEventListener('DOMContentLoaded', () => {

    renderNaviation();

    initRouter();

    window.addEventListener('popstate', ( e ) => {
        console.log('history change', e);
        initRouter();
    })


})