import Cinema from './pages/cinema';

const ROUTES = [
    {
        path: '/',
        component: Cinema,
        title: 'Cinema'
    }
]

export default ROUTES;