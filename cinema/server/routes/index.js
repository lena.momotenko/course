const Router = require('koa-router');
const router = new Router();

const DogApi = require('../api/dog');


router.get('/dogs', async (ctx) => {
    try {
        const result = await DogApi.getAllDogs();
        ctx.status = 200;
        ctx.body = result;
        console.log('fff');
    } catch (error) {
        console.log( 'err', error);
        ctx.status = 500;
        ctx.body = err || 'Internal Error'
    }
})

module.exports = router;
