
const Dog = require('../database/model/dog');

exports.createDog = ({ name, color, gender }) => new Promise( async (resolve, reject) => {
    try{
        const newDog = new Dog({
            name,
            color,
            gender
        });
        const createdDog = await newDog.save();
        resolve({
            success: true,
            data: createdDog
        })
    } 
    catch( err ){
        reject(err);
    } 
});

exports.getAllDogs = () => new Promise( async (resolve, reject) => {
    try {
        const allDogs = await Dog.find();
        resolve({
            success: true,
            data: allDogs
        })
    } catch (error) {
        console.log( error );
        reject(err);
    }
});


exports.getDog = ( _id ) => new Promise( async (resolve, reject) => {
    try {
        console.log('id', _id );
        const singleDog = await Dog.findById( _id );

        console.log('single', singleDog );
        resolve({
            success: true,
            data: singleDog
        })
    } catch (error) {
        console.log( error );
        reject(error);
    }
});

exports.deleteDog = ( _id ) => new Promise( async (resolve, reject) => {
    try {
        const removeDog = await Dog.deleteOne({ _id } );
        resolve({
            success: true,
            data: removeDog
        })

    } catch (error) {
        console.log( error );
        reject(error);
    }

});

