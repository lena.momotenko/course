const Koa = require('koa');
const koaBody = require('koa-body');
const serve = require('koa-static');
const koaCors = require('koa-cors');
const routes = require('./routes');

const app = new Koa();
const PORT = 3000;

require('./database');

app.use( serve('public') );
app.use( koaBody() );
app.use( koaCors() );

app.use( routes.routes() );
app.use( routes.allowedMethods() );

app.use( serve('public') );

console.log(`Server is running on port ${PORT}`)
app.listen(PORT);