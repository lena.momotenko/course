const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
    birth: {
        type: Date,
        default: new Date()
    },
    name: {
        type: String,
        required: ['true', 'Title is required']
    },
    color: {
        type: String,
        required: ['true', 'Color is required']
    },
    gender: {
        type: String,
        required: ['true', 'Gender is required']
    },
    breed: {
        type: String,
        default: null
    }
});

module.exports = mongoose.model('Dog', Schema );
