document.addEventListener('DOMContentLoaded', () => {
	let popupID;

  selector = {
		popup: {
			popupCustomize: document.getElementById('popupCustomize'),
			minmaxField: popupCustomize.querySelector('.js-minmax'),
			btnstyleField: popupCustomize.querySelector('.js-btnstyle'),
			addOptionField: popupCustomize.querySelector('.js-addOption'),
			addOptionBtn: popupCustomize.querySelector('.addOptionBtn'),
			deleteBtn: document.getElementById('delete'),
			closeBtn: document.getElementById('ok'),
			field: {
				changeText: document.getElementById('changeText'),
				changeDescription: document.getElementById('changeDescription'),
				changeColor: document.getElementById('changeColor'),
				changeBgColor: document.getElementById('changeBgColor'),
				max: document.getElementById('max'),
				min: document.getElementById('min'),
				addOption: document.getElementById('addOption'),
			}
		},
		result: document.getElementById('result'),
	}

	class createNewElement {
		constructor (params) {
			this.element = params.element;
			this._id = params._id ? params._id : Date.now();
			this.type = params.type || '';
			this.min = params.min || '';
			this.max = params.max || '';
			this.color = params.color || '';
			this.bgColor = params.bgColor || '';
			this.desc = params.desc || `Description for ${this.type} field`;
			this.text = params.text || this.type;
			this.options = params.options || [];
		}

		renderElement(element) {
			const elementHTML = {
				input: `
					<span class="desc js-desc">${this.desc}</span>
					<input type="${this.type}" class="input js-customize -${this.type}" id="${this._id}" readonly=readonly>
				`,
				btn: `
					<input type="${this.type}" class="input js-customize -${this.type}" value="${this.text}" id="${this._id}" readonly=readonly>
				`,
				textarea: `
					<span class="desc js-desc">${this.desc}</span>
					<textarea type="text" class="textarea js-customize" id="${this._id}" readonly=readonly></textarea>
				`,
				select: `
					<span class="desc js-desc">${this.desc}</span>
					<select type="${this.type}" id="${this._id}" class="input -${this.type} js-customize" readonly=readonly></select>
				`,
			}
			return elementHTML[element]
		}

		createNewField (parent = result) {
			let newElement = document.createElement('label');
			newElement.classList.add('label');
			newElement.setAttribute('for', this._id);
			newElement.innerHTML = this.renderElement(this.element);
			parent.appendChild(newElement);
			localStorage.setItem(this._id, JSON.stringify(this));
		}

		init () {
			this.createNewField();
		}
	}
	
	class newCustomElement {
		constructor(params) {
			this.element = params.element;
			this._id = params._id;
			this.type = params.type;
			this.min = params.min || '';
			this.max = params.max || '';
			this.color = params.color || '';
			this.bgColor = params.bgColor || '';
			this.text = params.text || this.type;
			this.options = params.options || [];
		}

		changeMinmax(target) {
			if (min) {
				target.setAttribute('min', this.min);
			};
			if (max != undefined) {
				target.setAttribute('max', this.max);
			};
		}
		
		changeColor(target) {
			target.style.color = this.color;
		}

		changeBgColor(target) {
			target.style.backgroundColor = this.bgColor;
		}

		changeDesc(target) {
			target.innerHTML = this.desc;
		}

		changeText(target) {
			target.value = this.text;
		}

		addNewOption(target) {
			let last = this.options.length - 1;		
			let newOption = document.createElement('option');
			newOption.innerHTML = this.options[last];
			target.appendChild(newOption);
		}

		addAllOption(target) {
			this.options.forEach(option => {
				let newOption = document.createElement('option');
				newOption.innerHTML = option;
				target.appendChild(newOption);
			});
		}

		showPopup (coordY) {
			const { popupCustomize, minmaxField, btnstyleField, addOptionField, addOptionBtn, deleteBtn, closeBtn  } = selector.popup;
			const { changeDescription, changeColor, changeBgColor, max, min, addOption } = selector.popup.field;
			const desc = document.querySelector('.js-desc');
			const target = document.getElementById(this._id);
			const label = selector.result.querySelector(`label[for='${this._id}']`);
			popupCustomize.style.top = `${coordY - 	popupCustomize.offsetHeight/2}px`;
			popupCustomize.classList.add('-active');
			
			
			if (/^text|password|number|email/.test(this.type)) {
				minmaxField.classList.add('-active');
			}

			if (/^submit|reset/.test(this.type)) {
				btnstyleField.classList.add('-active');
			}

			if (/^select/.test(this.type)) {
				addOptionField.classList.add('-active');
			}

			changeColor.addEventListener('change', () => {
				if (!changeColor.value) return;
				this.color = changeColor.value;
				this.changeColor(target);
			});
			
			changeBgColor.addEventListener('change', () => {
				if (!changeBgColor.value) return;
				this.bgColor = changeBgColor.value;
				this.changeBgColor(target);
			});	
			
			changeDescription.addEventListener('keyup', () => {
				this.desc = changeDescription.value;
				this.changeDesc(desc);
			});

			changeText.addEventListener('keyup', () => {
				if (!changeText.value) return;
				this.text = changeText.value;
				this.changeText(target);
			});

			addOptionBtn.addEventListener('click', () => {
				if(!addOption.value) return
				this.options.push(addOption.value);
				console.log(this.options);
				this.addNewOption(target);
				addOption.value = '';
			});
			
			deleteBtn.addEventListener('click', () => {
				localStorage.removeItem(this._id);
				label.remove();
				popupCustomize.classList.remove('-active');
			});

			closeBtn.addEventListener('click', () => {
				if (min.value || max.value) {
					min.value ? this.min = min.value : this.min = 0;
					max.value ? this.max = max.value : this.max = undefined;
					this.changeMinmax(target);
				}

				changeDescription.value = '';
				localStorage.setItem(this._id, JSON.stringify(this));
				popupCustomize.classList.remove('-active');
			});
		}

		addSttings() {
			const target = document.getElementById(this._id);
			const desc = document.querySelector('.js-desc');

			if (this.min || this.max) {
				this.changeMinmax(target);
			}

			if (this.color) {
				this.changeColor(target);
			}

			if (this.bgColor) {
				this.changeBgColor(target);
			}

			if (this.desc) {
				this.changeDesc(desc);
			}

			if (this.text) {
				this.changeText(target);
			}

			if (this.options) {
				this.addAllOption(target);
			}
		}

		init(coordY) {
			this.showPopup(coordY);
		}
	}

	/* start: render by onload */
	for (var key in localStorage) {
		let createdElementObject;
		if(localStorage.hasOwnProperty(key)) {
			if (localStorage[key][0] === '{') { /* Я устала, а иначе если в локалсторадже была запись с предыдущего занятия, ничего не работало */
				createdElementObject = JSON.parse(localStorage[key]);
				let newField = new createNewElement(createdElementObject);
				newField.init();
				let customize = new newCustomElement(createdElementObject);
				customize.addSttings();
			}
		}
	}

	document.querySelectorAll('.js-btn').forEach(btn => {
		btn.addEventListener('click', () => {
			let element = btn.dataset.element;
			let type = btn.dataset.type;
			let newField = new createNewElement({
				element: element, 
				type: type,
			});
			newField.init();
			addCustomizeEvent();
		});
	});

	function addCustomizeEvent () {
		document.querySelectorAll('.js-customize').forEach(field => {
			field.addEventListener('click', () => {
				let coordY = event.clientY; 
				let param = JSON.parse(localStorage.getItem(field.id));
				console.log('param:', param);
				let popup = new newCustomElement(param);
				popup.init(coordY);
			});
		});
	}

	addCustomizeEvent();
});

/* https://secure.wufoo.com/form-builder/ */