/**
 *
 * @param {object} params - Input params
 * @return DOMElement
 */
const createElement = params => {
  const { element, ...param } = params;
  const DOMElement = document.createElement(element);

  for (const name in param) {
    if (param[name]) {
      DOMElement.setAttribute(name, param[name]);
    }
  }

  if (/^(textarea)$/.test(element)) {
    DOMElement.placeholder = param.value;
  }

  if (/^(button)$/.test(element)) {
    DOMElement.innerText = param.value;
  }

  return DOMElement;
};

/**
 *
 * @param {object} parent - Parent DOM Element
 * @param {*} element - Child DOM Element
 */
const insertElement = (parent, element) => {
  parent.appendChild(element);
};

const possibleFields = {
  'min': '<input type="number" class="input" name="ChangeMin" id="changeMin">',
  'max': '<input type="number" class="input" name="ChangeMax" id="changeMax">',
  'color': '<input type="color" class="input" name="ChangeColor" id="changeColor">',
  'value': '<input type="text" class="input" name="ChangeText" id="changeText">',
  'desc': '<input type="text" class="input" name="AddDesc" id="addDesc">',
}

class Popup {
  constructor(template, fields) {
    this.popup = template;
    this.fields = fields;
  }

  showPopup() {
    this.popup.classList.remove("d-none");
    this.changeFields();
    console.log('click');
  }

  hidePopup() {
    this.popup.classList.add("d-none");
  }

  changeFields(fields) {
    this.popup.appendChild(possibleFields[color])
  }
}

// ******************************************************** //

/** Events */
const buttons = document.querySelectorAll(".js-btn");
const fieldsWrapper = document.querySelector(".js-fields-wrapper");

const popup = new Popup(".js-popup", ".js-popup-fields");

buttons.forEach(button => {
  button.addEventListener("click", event => {
    const {
      dataset: { type, element, value }
    } = event.currentTarget;

    const DOMElement = createElement({
      type,
      element,
      value,
      class: "form-control mb-2",
      id: Date.now()
    });

    DOMElement.addEventListener("click", popup.showPopup);

    insertElement(fieldsWrapper, DOMElement);
  });
});
