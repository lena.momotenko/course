document.addEventListener('DOMContentLoaded', () => {
  let selectors = {
    form: document.getElementById('comment'),
    author: document.getElementById('author'),
    message: document.getElementById('message'),
    writeMessageBtn: document.getElementById('_write-message'),
    popup: document.getElementById('popup'),
  }

  let globalData = {
    parentId: false,
  }

  class Message {
    constructor (user) {
      this._id = new Date().getTime();
      this.created_at = new Date();
      this.author = user.author;
      this.message = user.message;
      this.parentId = user.parentId ? user.parentId : null;
    }

    showPopup (popup) {
      popup.style.visibility = 'visible';
    }

    skipMessage (target, parent) {
      target.addEventListener('click', () => {
        parent.remove();
      });
    }

    answerMessage (target, parent) {
      target.addEventListener('click', e => {
        this.showPopup(selectors.popup);
        globalData.parentId = parent.id;
        this.parentId = parent.id;
      });
    }

    getData(date) {
      date = date ? date : new Date();
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}, ${date.getHours()}:${date.getMinutes()}`
    }

    createBlock (html, className, parent) {
      const newPost = document.createElement('li');
      newPost.classList.add(className);
      newPost.setAttribute('id', this._id)
      newPost.innerHTML = html;
      parent.appendChild(newPost);
      const skipBtn = newPost.querySelector('._skipMessage');
      const answerBtn = newPost.querySelector('._answerMessage');
      this.skipMessage(skipBtn, newPost);
      this.answerMessage(answerBtn, newPost);
    }
    
    renderMessage() {
      console.log(this);
      const answers = document.getElementById('answers');
      const root = answers.querySelector('.message_list');
      const idParentId = this.parentId ? `| Parent ID: #${this.parentId}` : '';
      const newAnswerHTML = `
        <h6 class='message__date'>Posted ${this.getData(this.created_at)} | ID: #${this._id} ${idParentId}</h6>
        <h4 class='message__author'>${this.author}</h4>
        <p class='message__text'>${this.message}</p>
        <input class='_skipMessage' type='button' value='Skip'>
        <input class='_answerMessage' type='button' value='Answer'>
      `;
      this.createBlock(newAnswerHTML, 'answer', root);
    }

    sendAnswer() {
      this.renderMessage(this);
    }
  }

  selectors.writeMessageBtn.addEventListener('click', () => {
    selectors.popup.style.visibility = 'visible';
  });
  
  selectors.form.addEventListener('submit', e => {
    e.preventDefault();
    let message;
    if(globalData.parentId) {
      message = new Message({
        author: selectors.author.value,
        message: selectors.message.value,
        parentId: globalData.parentId,
      });
    } else {
      message = new Message({
        author: selectors.author.value,
        message: selectors.message.value,
      });
    }
    message.sendAnswer();
    selectors.message.value = '';
    selectors.popup.style.visibility = 'hidden';
    globalData.parentId = false;
  });

/*
  Задание на классы.

  Создать класс Message конструктор котрого состоит из следующих полей:
  id, author, text, date, answers

  У этого класса будет два метода:
  SkipMessage() -> которое должно найти сообщение и удалить его из массива.
  AnswerMessage()-> должно отрыть поле ввода, в который вы сможете ввести ответ
  SendAnswer() -> метод который «отравляет» ответ, при этом обновляя поле ответа(answers) в вашем объекте с сообщением.

  В метод SendAnswer нужно передать обьект класса Answer который наследуется от класса Message,
  Но еще дополнительно будет иметь поле parentId.


  message = {
    id: '',
    author: '',
    text: '',
    date: '',
    answers: [
      {
        id: '',
        author: '',
        text: '',
        date: '',
        parentId: '',
        answers: ''
      }
    ]
  }

  +Bonus: После создании сообщение добавляется в массив со всеми сообщениями, которые потом будут выводится на экран.

  _ _ _

  Задание можно разбелить на две части:
    1. Создание самого сообщения:
    id->выдаётся автоматом
    Author-> селект с выбором автора
    Text -> текст сообщения
    date -> автоматически в момент создания

    Все данные беруться с формы. Массив с сообщениями передается в функцию рендера, которая генерирует сам список.

    2. Ответ на сообщение

*/
});