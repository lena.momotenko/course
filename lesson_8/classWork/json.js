'use strict';
document.addEventListener('DOMContentLoaded', () => {

  const loginForm = document.forms.login;  
  const resultForm = document.forms.result;  
  const hiddenField = resultForm.elements.json;

  loginForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let serialized = [];
    let field = e.target.elements;
    serialized = Array.from(field);
    serialized = serialized.map(function(item) {
      if(item.type !== 'button') {
        if(item.value != undefined) {
          return item.value;
        }
      }
    });
    hiddenField.value = JSON.stringify(serialized);
  });

  resultForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let resulValue = JSON.parse(hiddenField.value);
    if (resulValue) {
      hiddenField.innerHTML = resulValue;
      hiddenField.hidden = false;
    }
  });
  
});


/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'



*/
