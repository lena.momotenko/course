export function createElement (element = 'div', className = '', text = '', parent = document) {
   let newElement = document.createElement(element);
   newElement.classList.add(className);
   newElement.innerText = text;
   parent.appendChild(newElement);
}